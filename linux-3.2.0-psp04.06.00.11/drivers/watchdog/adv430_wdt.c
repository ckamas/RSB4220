/*
 * Advantech Single Board Computer WDT driver
 *
 * (c) Copyright 2013-2014 Marek Michalkiewicz <marekm@linux.org.pl>
 *
 * Written by Ryan xin <jinlong.xin@advantech.com.cn>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/watchdog.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/uaccess.h>
#include <linux/i2c.h>
#include <linux/gpio.h>
#include <linux/of_gpio.h>

#define ADVWDT_TIMEOUT_REG	0x15
#define ADVWDT_PRETIME_REG	0x16
#define ADVWDT_TIMEREMAIN_REG	0x25
#define ADVWDT_PREREMAIN_REG	0x26
#define ADVWDT_FWVERSION_REG	0x27
#define ADVWDT_TIMEOUT_BASE	10

#define GPIO_TO_PIN(bank, gpio) (32 * (bank) + (gpio))

#ifdef CONFIG_MACH_AM335X_RSB4220
#define WDI_GPIO_ID GPIO_TO_PIN(2,4)
#define WDT_GPIO_ID GPIO_TO_PIN(2,0)
#endif

#ifdef CONFIG_MACH_AM335X_ROM3310
#define WDI_GPIO_ID GPIO_TO_PIN(2,5)
#define WDT_GPIO_ID GPIO_TO_PIN(3,4)
#endif

static const struct i2c_device_id adv430wdt_id[] = {
	{ "adv430_wdt", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, adv430wdt_id);

struct adv430_wdt {
	struct i2c_client 	*client;
	struct miscdevice	miscdev;
	int			timer_margin;
};

static struct adv430_wdt *wdt;

static int nowayout = WATCHDOG_NOWAYOUT;
module_param(nowayout, int, 0);
MODULE_PARM_DESC(nowayout, "Watchdog cannot be stopped once started "
	"(default=" __MODULE_STRING(WATCHDOG_NOWAYOUT) ")");


static void adv430_wdt_enable(struct adv430_wdt *wdt)
{
	if (gpio_is_valid(WDT_GPIO_ID))
	{
		gpio_set_value(WDT_GPIO_ID, 1);
	}

	return;
}

static void adv430_wdt_disable(struct adv430_wdt *wdt)
{
	if (gpio_is_valid(WDT_GPIO_ID))
	{
		gpio_set_value(WDT_GPIO_ID, 0);
	}

	return;
}

static void adv430_wdt_ping(struct adv430_wdt *wdt)
{
	unsigned flag  = 0;

	if (gpio_is_valid(WDI_GPIO_ID))
	{
		flag = gpio_get_value(WDI_GPIO_ID);
		msleep(1);
		gpio_set_value(WDI_GPIO_ID, (flag^1));
	}

	return;
}

static int adv430_wdt_set_timeout(struct i2c_client *wdt_client, u32 val)
{
	int ret = 0;

	if(val < 0 || val > 0x0ffff)
		val = 60;
	ret = i2c_smbus_write_word_data(wdt_client, ADVWDT_TIMEOUT_REG, (val * ADVWDT_TIMEOUT_BASE));
	if(ret < 0)
	{
		printk("Watch dog set timeout failed!\n");
	}

	return ret;
}

static int adv430_wdt_get_timeout(struct i2c_client *wdt_client)
{
	int ret = 0;

	ret = i2c_smbus_read_word_data(wdt_client, ADVWDT_TIMEOUT_REG);
	if(ret < 0)
	{
		printk("Watch dog get timeout failed!\n");
	}

	ret = ((ret & 0x0FFFF) / ADVWDT_TIMEOUT_BASE);

	return ret;
}

static long adv430_wdt_ioctl(struct file *file,
		unsigned int cmd, unsigned long arg)
{
	void __user *argp = (void __user *)arg;
	int __user *p = argp;
	int new_margin;

	static const struct watchdog_info adv430_wd_ident = {
		.identity = "ADV430 Watchdog",
		.options = WDIOF_KEEPALIVEPING | WDIOF_SETTIMEOUT | WDIOF_MAGICCLOSE,
		.firmware_version = 0,
	};

	switch (cmd) {
	case WDIOC_GETSUPPORT:
		return copy_to_user(argp, &adv430_wd_ident,
				sizeof(adv430_wd_ident)) ? -EFAULT : 0;

	case WDIOC_GETSTATUS:
	case WDIOC_GETBOOTSTATUS:
		return put_user(0, p);
	
	case WDIOC_SETOPTIONS:
	{
		int options, retval = -EINVAL;

		if (get_user(options, p))
			return -EFAULT;
		if (options & WDIOS_DISABLECARD) {
			adv430_wdt_disable(wdt);
			retval = 0;
		}
		if (options & WDIOS_ENABLECARD) {
			adv430_wdt_ping(wdt);
			retval = 0;
		}
		return retval;
	}
	
	case WDIOC_KEEPALIVE:
		adv430_wdt_ping(wdt);
		break;

	case WDIOC_SETTIMEOUT:
		if (get_user(new_margin, p))
			return -EFAULT;
		adv430_wdt_disable(wdt);
		wdt->timer_margin = new_margin;
		if (adv430_wdt_set_timeout(wdt->client, new_margin))
			return -EINVAL;
		adv430_wdt_enable(wdt);
		msleep(50);
		adv430_wdt_ping(wdt);
		return put_user(wdt->timer_margin, p);

	case WDIOC_GETTIMEOUT:
		wdt->timer_margin = adv430_wdt_get_timeout(wdt->client);
		msleep(50);
		return put_user(wdt->timer_margin, p);

	default:
		return -ENOTTY;
	}

	return 0;
}

static int adv430_wdt_open(struct inode *inode, struct file *file)
{
	return 0;
}

static int adv430_wdt_release(struct inode *inode, struct file *file)
{
	return 0;
}

static ssize_t adv430_wdt_write(struct file *file,
		const char __user *data, size_t len, loff_t *ppos)
{
	return len;
}

static const struct file_operations adv430_wdt_fops = {
	.owner = THIS_MODULE,
	.write = adv430_wdt_write,
	.unlocked_ioctl = adv430_wdt_ioctl,
	.open = adv430_wdt_open,
	.release = adv430_wdt_release,
	.llseek = no_llseek,
};

static struct i2c_driver adv430wdt_driver;

static int adv430wdt_probe(struct i2c_client *client,
			 const struct i2c_device_id *id)
{
	int ret, err;
	
	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) {
		err = -ENODEV;
		return err;
	}

	wdt = kzalloc(sizeof(struct adv430_wdt), GFP_KERNEL);
	if (!wdt) {
		err = -ENOMEM;
		kfree(wdt);
		return err;
	}

	wdt->timer_margin	= 60;
	wdt->miscdev.parent	= NULL;
	wdt->miscdev.fops	= &adv430_wdt_fops;
	wdt->miscdev.minor	= WATCHDOG_MINOR;
	wdt->miscdev.name	= "watchdog";

	wdt->client = client;
	i2c_set_clientdata(wdt->client, wdt);

	adv430_wdt_disable(wdt);
	/* Add WDI HIGH----LOW */
	if (gpio_is_valid(WDI_GPIO_ID))
	{
		gpio_set_value(WDI_GPIO_ID, 1);
		msleep(100);
		gpio_set_value(WDI_GPIO_ID, 0);
	}

	ret = misc_register(&wdt->miscdev);
	if (ret) {
		dev_err(wdt->miscdev.parent,
			"Failed to register misc device\n");
		i2c_unregister_device(wdt->client);
		kfree(wdt);
		return ret;
	}

	return 0;	
}

static int adv430wdt_remove(struct i2c_client *client)
{
	struct adv430_wdt *wdt = i2c_get_clientdata(client);
	
	i2c_unregister_device(wdt->client);

	misc_deregister(&wdt->miscdev);

	kfree(wdt);

	return 0;
}

static void adv430wdt_shutdown(struct i2c_client *client)
{
	struct adv430_wdt *wdt = i2c_get_clientdata(client);
	adv430_wdt_disable(wdt->client);
	adv430_wdt_ping(wdt->client);

	return;
}

static struct i2c_driver adv430wdt_driver = {
	.driver = {
        .name = "adv430wdt",
	},
	.probe = adv430wdt_probe,
	.remove = adv430wdt_remove,
	.shutdown = adv430wdt_shutdown,
	.id_table = adv430wdt_id,
};

static int __init adv430wdt_init(void)
{
	return i2c_add_driver(&adv430wdt_driver);
}

static void __exit adv430wdt_exit(void)
{
	i2c_del_driver(&adv430wdt_driver);
}

module_init(adv430wdt_init);
module_exit(adv430wdt_exit);
MODULE_AUTHOR("Advantech Corporation");
MODULE_LICENSE("GPL");
MODULE_ALIAS_MISCDEV(WATCHDOG_MINOR);
MODULE_ALIAS("platform:adv430_wdt");
