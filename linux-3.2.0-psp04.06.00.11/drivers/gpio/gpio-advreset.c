/*
 * advantech GPIO reset function for WISE3320
 *
 * Copyright 2014 Advantech Corp.
 *
 * Author: Jianfeng <Jianfeng@advantech.com.cn>
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 */

#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/gpio.h>
#include <linux/mfd/core.h>
#include <linux/platform_device.h>
#include <linux/seq_file.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/reboot.h>

#define GPIO_TO_PIN(bank, gpio) (32 * (bank) + (gpio))

static irqreturn_t advreset_gpio_handler(int irq, void *dev_id)
{

	if (gpio_get_value(GPIO_TO_PIN(3, 17)) == 0) //falling
	{
		*(u64 *)dev_id = (u64)jiffies;
		//machine_restart(NULL);
	}
	else if (gpio_get_value(GPIO_TO_PIN(3, 17)) == 1) //raising
	{
		if ((jiffies - *((u64 *)dev_id)) >= 4 * HZ )
		{
			machine_restart(NULL);
		}
	}
	return IRQ_HANDLED;
}



static int __devinit advreset_gpio_probe(struct platform_device *pdev)
{
	struct resource *res;
	int ret;
	
    	printk("Init reset gpio\n");
        res = platform_get_resource(pdev, IORESOURCE_IRQ, 0);

        if (!res) {
                dev_err(&pdev->dev, "error getting irq res\n");
                ret = -ENOENT;
                goto err;
        }

        
	ret = request_irq(res->start, advreset_gpio_handler,
			     0, "advreset-gpio", pdev->dev.platform_data);
	if (ret != 0) {
		dev_err(&pdev->dev,
			"%s request_irq failed\n", __func__);
		goto err;
	}

	//platform_set_drvdata(pdev, advreset_gpio);
err:
	return ret;

}

static int __devexit advreset_gpio_remove(struct platform_device *pdev)
{
	//struct advreset_gpio *advreset_gpio = platform_get_drvdata(pdev);
	//int ret;

	//ret = gpiochip_remove(&wm831x_gpio->gpio_chip);
	//if (ret == 0)
	//	kfree(advreset_gpio);

	return 0;
}

static struct platform_driver advreset_gpio_driver = {
	.driver.name	= "advreset-gpio",
	.driver.owner	= THIS_MODULE,
	.probe		= advreset_gpio_probe,
	.remove		= __devexit_p(advreset_gpio_remove),
};

static int __init advreset_gpio_init(void)
{
	return platform_driver_register(&advreset_gpio_driver);
}


static void __exit advreset_gpio_exit(void)
{
	platform_driver_unregister(&advreset_gpio_driver);
}
module_init(advreset_gpio_init);
module_exit(advreset_gpio_exit);

MODULE_AUTHOR("Jianfeng <Jianfeng.dai@advantech.com.cn>");
MODULE_DESCRIPTION("GPIO interface for Advantech WISE3320 reset");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:advreset-gpio");
