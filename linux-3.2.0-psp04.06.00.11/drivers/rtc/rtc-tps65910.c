/*
 * rtc-tps65910.c -- TPS65910 Real Time Clock interface
 * Copyright (C) 2014 advantech corp
 * Author: Ryan.xin <jinlong.xin@advantech.com.cn>
 *
 * Based on original TI driver twl-rtc.c
 *   Copyright (C) 2007 Texas Instruments, Inc.

 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version
 * 2 of the License, or (at your option) any later version.
 */

#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/rtc.h>
#include <linux/bcd.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/mfd/tps65910.h>


/* Total number of RTC registers needed to set time*/
#define NUM_TIME_REGS        (TPS65910_YEARS - TPS65910_SECONDS + 1)

/* RTC_CTRL_REG bitfields */
#define TPS65910_RTC_CTRL_STOP_RTC              0x01 /*0=stop, 1=run */
#define TPS65910_RTC_CTRL_GET_TIME              0x40

/* RTC_STATUS_REG bitfields */
#define TPS65910_RTC_STATUS_ALARM               0x40

/* RTC_INTERRUPTS_REG bitfields */
#define TPS65910_RTC_INTERRUPTS_EVERY           0x03
#define TPS65910_RTC_INTERRUPTS_IT_TIMER        0x04
#define TPS65910_RTC_INTERRUPTS_IT_ALARM        0x08
#define TPS65910_RTC_STATUS_REG_POWER_UP        0x80


/*
 * Supports 1 byte read from TPS65910 RTC register.
 */
static int tps65910_rtc_read_u8(struct tps65910 * tps, u8 reg, u8 *data)
{
    return tps->read(tps, reg, 1, data);
}

/*
 * Supports 1 byte write to TPS65910 RTC registers.
 */
static int tps65910_rtc_write_u8(struct tps65910 * tps, u8 reg, u8 val )
{
    return tps->write(tps, reg, 1, &val);
}

static int tps65910_read_array(struct tps65910 * tps,u8 reg, int size, char *data )
{
    return tps->read(tps, reg, size, data);
}

static int tps65910_write_array(struct tps65910 * tps,u8 reg, int size, char * data )
{
    return tps->write(tps, reg, size, data);
}


/*
 * Cache the value for timer/alarm interrupts register; this is
 * only changed by callers holding rtc ops lock (or resume).
 */
static unsigned char rtc_irq_bits;

/*
 * Enable 1/second update and/or alarm interrupts.
 */
static int set_rtc_irq_bit(struct tps65910 * tps,unsigned char bit)
{
    unsigned char val;
    int ret;

    val = rtc_irq_bits | bit;
    val &= ~TPS65910_RTC_INTERRUPTS_EVERY;
    ret = tps65910_rtc_write_u8(tps, TPS65910_RTC_INTERRUPTS, val);
    if (ret == 0)
        rtc_irq_bits = val;

    return ret;
}

/*
 * Disable update and/or alarm interrupts.
 */
static int mask_rtc_irq_bit(struct tps65910 * tps, unsigned char bit)
{
    unsigned char val;
    int ret;

    val = rtc_irq_bits & ~bit;
    ret = tps65910_rtc_write_u8(tps,TPS65910_RTC_INTERRUPTS, val);
    if (ret == 0)
        rtc_irq_bits = val;

    return ret;
}


static int tps65910_rtc_alarm_irq_enable(struct device *dev, unsigned enabled)
{
    struct tps65910 *tps = dev_get_drvdata(dev->parent);
    int ret;

    if (enabled)
        ret = set_rtc_irq_bit(tps, TPS65910_RTC_INTERRUPTS_IT_ALARM);
    else
        ret = mask_rtc_irq_bit(tps, TPS65910_RTC_INTERRUPTS_IT_ALARM);
    return ret;

}

/*
 * Gets current tps65910 RTC time and date parameters.
 *
 * The RTC's time/alarm representation is not what gmtime(3) requires
 * Linux to use:
 *
 *  - Months are 1..12 vs Linux 0-11
 *  - Years are 0..99 vs Linux 1900..N (we assume 21st century)
 */
static int tps65910_rtc_read_time(struct device *dev, struct rtc_time *tm)
{
    unsigned char rtc_data[NUM_TIME_REGS];
    struct tps65910 *tps = dev_get_drvdata(dev->parent);
    int ret;

    tps65910_set_bits(tps, TPS65910_RTC_CTRL, TPS65910_RTC_CTRL_GET_TIME);

    ret = tps65910_read_array(tps, TPS65910_SECONDS,
                              NUM_TIME_REGS, rtc_data);
    if (ret < 0) {
        dev_err(dev, "reading from RTC failed with err:%d\n", ret);
        return ret;
    }

    tm->tm_sec = bcd2bin(rtc_data[0]);
    tm->tm_min = bcd2bin(rtc_data[1]);
    tm->tm_hour = bcd2bin(rtc_data[2]);
    tm->tm_mday = bcd2bin(rtc_data[3]);
    tm->tm_mon = bcd2bin(rtc_data[4]) - 1;
    tm->tm_year = bcd2bin(rtc_data[5]) + 100;

    return ret;
}

static int tps65910_rtc_set_time(struct device *dev, struct rtc_time *tm)
{
    unsigned char rtc_data[NUM_TIME_REGS];
    struct tps65910 *tps = dev_get_drvdata(dev->parent);
    int ret;
    
    rtc_data[0] = bin2bcd(tm->tm_sec);
    rtc_data[1] = bin2bcd(tm->tm_min);
    rtc_data[2] = bin2bcd(tm->tm_hour);
    rtc_data[3] = bin2bcd(tm->tm_mday);
    rtc_data[4] = bin2bcd(tm->tm_mon + 1);
    rtc_data[5] = bin2bcd(tm->tm_year - 100);

    /* Stop RTC while updating the RTC time registers */
    ret = tps65910_clear_bits(tps,TPS65910_RTC_CTRL,TPS65910_RTC_CTRL_STOP_RTC);
    if (ret < 0) {
        dev_err(dev, "RTC stop failed with err:%d\n", ret);
        goto out;
    }

    /* update all the time registers in one shot */
    ret = tps65910_write_array(tps, TPS65910_SECONDS,
                               NUM_TIME_REGS,rtc_data);
    if (ret < 0) {
        dev_err(dev, "rtc_set_time error %d\n", ret);
        goto out;
    }

    /* Start back RTC */
    ret = tps65910_set_bits(tps,TPS65910_RTC_CTRL,TPS65910_RTC_CTRL_STOP_RTC);
    if (ret < 0)
        dev_err(dev, "RTC start failed with err:%d\n", ret);
out:
    return ret;
}

/*
 * Gets current tps65910 RTC alarm time.
 */
static int tps65910_rtc_read_alarm(struct device *dev, struct rtc_wkalrm *alm)
{
    unsigned char alarm_data[NUM_TIME_REGS];

    struct tps65910 *tps = dev_get_drvdata(dev->parent);
    int ret;

    ret = tps65910_read_array(tps, TPS65910_SECONDS,
                              NUM_TIME_REGS, alarm_data);
    if (ret < 0) {
        dev_err(dev, "rtc_read_alarm error %d\n", ret);
        return ret;
    }

    alm->time.tm_sec = bcd2bin(alarm_data[0]);
    alm->time.tm_min = bcd2bin(alarm_data[1]);
    alm->time.tm_hour = bcd2bin(alarm_data[2]);
    alm->time.tm_mday = bcd2bin(alarm_data[3]);
    alm->time.tm_mon = bcd2bin(alarm_data[4]) - 1;
    alm->time.tm_year = bcd2bin(alarm_data[5]) + 100;

    /* report cached alarm enable state */
    if (rtc_irq_bits & TPS65910_RTC_INTERRUPTS_IT_ALARM)
        alm->enabled = 1;

    return ret;
}

static int tps65910_rtc_set_alarm(struct device *dev, struct rtc_wkalrm *alm)
{
    unsigned char alarm_data[NUM_TIME_REGS];
    struct tps65910 *tps = dev_get_drvdata(dev->parent);
    int ret;

    ret = tps65910_rtc_alarm_irq_enable(dev, 0);
    if (ret)
        goto out;

    alarm_data[0] = bin2bcd(alm->time.tm_sec);
    alarm_data[1] = bin2bcd(alm->time.tm_min);
    alarm_data[2] = bin2bcd(alm->time.tm_hour);
    alarm_data[3] = bin2bcd(alm->time.tm_mday);
    alarm_data[4] = bin2bcd(alm->time.tm_mon + 1);
    alarm_data[5] = bin2bcd(alm->time.tm_year - 100);

    /* update all the alarm registers in one shot */
    ret = tps65910_write_array(tps, TPS65910_ALARM_SECONDS,
                               NUM_TIME_REGS,alarm_data);
    if (ret) {
        dev_err(dev, "rtc_set_alarm error %d\n", ret);
        goto out;
    }

    if (alm->enabled)
        ret = tps65910_rtc_alarm_irq_enable(dev, 1);
out:
    return ret;

}

static irqreturn_t tps65910_rtc_interrupt(int irq, void *rtc)
{
    struct device *dev = rtc;
    struct tps65910 *tps = dev_get_drvdata(dev->parent);
    unsigned long events = 0;
    int ret = IRQ_NONE;
    u8 rd_reg;

    ret = tps65910_rtc_read_u8(tps, TPS65910_RTC_STATUS, &rd_reg);
    if (ret)
        goto out;
    /*
    * Figure out source of interrupt: ALARM or TIMER in RTC_STATUS_REG.
    * only one (ALARM or RTC) interrupt source may be enabled
    * at time, we also could check our results
     * by reading RTS_INTERRUPTS_REGISTER[IT_TIMER,IT_ALARM]
    */

    if (rd_reg & TPS65910_RTC_STATUS_ALARM)
        events = RTC_IRQF | RTC_AF;
    else
        events |= RTC_IRQF | RTC_UF;

    ret = tps65910_rtc_write_u8(tps, TPS65910_RTC_STATUS, rd_reg | TPS65910_RTC_STATUS_ALARM );
    if (ret)
        goto out;

    /* Notify RTC core on event */
    rtc_update_irq(rtc, 1, events);
    ret = IRQ_HANDLED;
out:
    return ret;
}

static struct rtc_class_ops tps65910_rtc_ops = {
    .read_time        = tps65910_rtc_read_time,
    .set_time        = tps65910_rtc_set_time,
    .read_alarm        = tps65910_rtc_read_alarm,
    .set_alarm        = tps65910_rtc_set_alarm,
    .alarm_irq_enable = tps65910_rtc_alarm_irq_enable,
};

static int __devinit tps65910_rtc_probe(struct platform_device *pdev)
{
    struct tps65910 *tps65910 = NULL;
    struct rtc_device *rtc;

    int ret = -EINVAL;
    int irq = platform_get_irq(pdev, 0);
    u8 rd_reg;
    
    tps65910 = dev_get_drvdata(pdev->dev.parent);
    /*
    if (irq <= 0)
    	goto out1;
    */

    /* Clear pending interrupts */
    ret = tps65910_rtc_read_u8(tps65910, TPS65910_RTC_STATUS, &rd_reg);
    if (ret < 0)
        goto out1;
    if (rd_reg & TPS65910_RTC_STATUS_REG_POWER_UP)
        dev_warn(&pdev->dev, "Power up reset detected.\n");

    if (rd_reg & TPS65910_RTC_STATUS_ALARM)
        dev_warn(&pdev->dev, "Pending Alarm interrupt detected.\n");

    ret = tps65910_rtc_write_u8(tps65910, TPS65910_RTC_STATUS, rd_reg);
    if (ret < 0)
        goto out1;

    /* Check RTC module status, Enable if it is off */
    ret = tps65910_rtc_read_u8(tps65910, TPS65910_RTC_CTRL, &rd_reg);
    if (ret < 0)
        goto out1;
    dev_dbg(&pdev->dev, "Enabling tps65910-RTC.\n");
    if (!(rd_reg & TPS65910_RTC_CTRL_STOP_RTC)) {
        rd_reg = TPS65910_RTC_CTRL_STOP_RTC;
        ret = tps65910_rtc_write_u8(tps65910, TPS65910_RTC_CTRL, rd_reg);
        if (ret < 0)
            goto out1;
    }
    /*Check if have external oscillator open*/
    ret = tps65910_rtc_read_u8(tps65910, TPS65910_DEVCTRL, &rd_reg);
    if (ret < 0)
        goto out1;
    ret = tps65910_rtc_write_u8(tps65910, TPS65910_DEVCTRL, (rd_reg & 0X9F));
        if (ret < 0)
            goto out1;


    /* init cached IRQ enable bits */
    ret = tps65910_rtc_read_u8(tps65910, TPS65910_RTC_INTERRUPTS, &rtc_irq_bits);
    if (ret < 0)
        goto out1;

    rtc = rtc_device_register(pdev->name, &pdev->dev,
                              &tps65910_rtc_ops, THIS_MODULE);
    if (IS_ERR(rtc)) {
        ret = PTR_ERR(rtc);
        dev_err(&pdev->dev, "can't register RTC device, err %ld\n",
                PTR_ERR(rtc));
        goto out1;

    }
    /*
         ret = request_threaded_irq(irq, NULL, tps65910_rtc_interrupt,
    		IRQF_TRIGGER_RISING,
             dev_name(&rtc->dev), rtc);
         if (ret < 0) {
                 dev_err(&pdev->dev, "IRQ is not free.\n");
    		goto out2;
         }
    */
    platform_set_drvdata(pdev, rtc);

    return 0;
out2:
    rtc_device_unregister(rtc);
out1:
    return ret;
}

/*
 * Disable all tps65910 RTC module interrupts.
 * Sets status flag to free.
 */
static int __devexit tps65910_rtc_remove(struct platform_device *pdev)
{
    /* leave rtc running, but disable irqs */
    struct rtc_device *rtc = platform_get_drvdata(pdev);
    struct tps65910 *tps = dev_get_drvdata(pdev->dev.parent);
    int irq = platform_get_irq(pdev, 0);
    mask_rtc_irq_bit(tps,TPS65910_RTC_INTERRUPTS_IT_ALARM);
    mask_rtc_irq_bit(tps,TPS65910_RTC_INTERRUPTS_IT_TIMER);

    free_irq(irq, rtc);
    rtc_device_unregister(rtc);
    platform_set_drvdata(pdev, NULL);
    return 0;
}
static void tps65910_rtc_shutdown(struct platform_device *pdev)
{
    /* mask timer interrupts, but leave alarm interrupts on to enable
       power-on when alarm is triggered */
    struct tps65910 *tps = dev_get_drvdata(pdev->dev.parent);
    mask_rtc_irq_bit(tps, TPS65910_RTC_INTERRUPTS_IT_TIMER);
}
#ifdef CONFIG_PM

static unsigned char irqstat;
static int tps65910_rtc_suspend(struct platform_device *pdev, pm_message_t state)
{
    struct tps65910 *tps = dev_get_drvdata(pdev->dev.parent);
    irqstat = rtc_irq_bits;
    mask_rtc_irq_bit(tps, TPS65910_RTC_INTERRUPTS_IT_TIMER );
    return 0;
}

static int tps65910_rtc_resume(struct platform_device *pdev)
{
    struct tps65910 *tps = dev_get_drvdata(pdev->dev.parent);

    /* Restore list of enabled interrupts before suspend */
    set_rtc_irq_bit(tps,irqstat);
    return 0;
}
#else
#define twl_rtc_suspend NULL
#define twl_rtc_resume  NULL
#endif

static struct platform_driver tps65910rtc_driver = {
    .probe          = tps65910_rtc_probe,
    .remove         = __devexit_p(tps65910_rtc_remove),
    .shutdown		= tps65910_rtc_shutdown,
    .suspend        = tps65910_rtc_suspend,
    .resume         = tps65910_rtc_resume,
    .driver                = {
        .owner        = THIS_MODULE,
        .name        = "tps65910-rtc",
    },
};
static int __init tps65910_rtc_init(void)
{
    return platform_driver_register(&tps65910rtc_driver);
}
module_init(tps65910_rtc_init);

static void __exit tps65910_rtc_exit(void)
{
    platform_driver_unregister(&tps65910rtc_driver);
}
module_exit(tps65910_rtc_exit);

MODULE_ALIAS("platform:tps65910_rtc");
MODULE_AUTHOR("Ryan <jinlong.xin@advantech.com.cn>");
MODULE_LICENSE("GPL");
