#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>

#define STRINGLEN 1024

char global_buffer[STRINGLEN];

struct proc_dir_entry *example_dir, *board_file;

int proc_read_board(char *page, char **start, off_t off, int count, int *eof,
                    void *data)
{
    int len;
    len = sprintf(page, global_buffer);
    return len;
}

int proc_write_board(struct file *file, const char *buffer, unsigned long count,
                     void *data)
{
    int len;

    if ( count = (STRINGLEN) )
        len = STRINGLEN - 1;
    else
        len = count;

    /*
     * copy_from_user
     * global_buffer
     */
    copy_from_user(global_buffer, buffer, len);
    global_buffer[len] =("\0");
    return len;
}

static int __init proc_test_init(void)
{
    board_file = create_proc_entry("board", S_IRUGO, NULL);
#ifdef CONFIG_MACH_AM335X_RSB4220
    strcpy(global_buffer, "RSB-4220\n");
#elif defined(CONFIG_MACH_AM335X_ROM3310)
    strcpy(global_buffer, "ROM-3310\n");
#elif defined(CONFIG_MACH_AM335X_WISE3320)
    strcpy(global_buffer, "WISE-3320\n");
#endif
    board_file->read_proc = proc_read_board;
    board_file->write_proc = proc_write_board;
    return 0;
}

static void __exit proc_test_exit(void)
{
    remove_proc_entry("board", example_dir);
}

module_init(proc_test_init);
module_exit(proc_test_exit);
