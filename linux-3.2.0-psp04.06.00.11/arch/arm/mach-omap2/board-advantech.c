/*
 * Code for AM335X EVM.
 *
 * Copyright (C) 2011 Texas Instruments, Inc. - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/module.h>
#include <linux/i2c/at24.h>
#ifdef CONFIG_MACH_ADVANTECH
#include <linux/i2c/pca953x.h>
#endif
#include <linux/phy.h>
#include <linux/gpio.h>
#include <linux/spi/spi.h>
#include <linux/irq.h>
#include <linux/spi/flash.h>
#include <linux/gpio_keys.h>
#include <linux/input.h>
#include <linux/input/matrix_keypad.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/nand.h>
#include <linux/mtd/partitions.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/export.h>
#include <linux/wl12xx.h>
#include <linux/ethtool.h>
#include <linux/mfd/tps65910.h>
#include <linux/mfd/tps65217.h>
#include <linux/pwm_backlight.h>
#include <linux/input/ti_tsc.h>
#include <linux/platform_data/ti_adc.h>
#include <linux/mfd/ti_tscadc.h>
#include <linux/reboot.h>
#include <linux/pwm/pwm.h>
#include <linux/rtc/rtc-omap.h>
#include <linux/opp.h>

/* LCD controller is similar to DA850 */
#include <video/da8xx-fb.h>

#include <mach/hardware.h>
#include <mach/board-am335xevm.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/hardware/asp.h>

#include <plat/omap_device.h>
#include <plat/omap-pm.h>
#include <plat/irqs.h>
#include <plat/board.h>
#include <plat/common.h>
#include <plat/lcdc.h>
#include <plat/usb.h>
#include <plat/mmc.h>
#include <plat/emif.h>
#include <plat/nand.h>

#include "board-flash.h"
#include "cpuidle33xx.h"
#include "devices.h"
#include "hsmmc.h"

#ifdef CONFIG_MACH_AM335X_RSB4220
#include "board-rsb4220.h"
#elif defined(CONFIG_MACH_AM335X_ROM3310)
#include "board-rom3310.h"
#elif defined(CONFIG_MACH_AM335X_WISE3320)
#include "board-wise3320.h"
#endif

/* BBB PHY IDs */
#define BBB_PHY_ID		0x7c0f1
#define BBB_PHY_MASK		0xfffffffe

/* AM335X EVM Phy ID and Debug Registers */
#define AM335X_EVM_PHY_ID		0x4dd074
#define AM335X_EVM_PHY_MASK		0xfffffffe
#define AR8051_PHY_DEBUG_ADDR_REG	0x1d
#define AR8051_PHY_DEBUG_DATA_REG	0x1e
#define AR8051_DEBUG_RGMII_CLK_DLY_REG	0x5
#define AR8051_RGMII_TX_CLK_DLY		BIT(8)

static const struct display_panel disp_panel = {
	WVGA,
	32,
	32,
	COLOR_ACTIVE,
};

/* LCD backlight platform Data */
#define AM335X_BACKLIGHT_MAX_BRIGHTNESS        100
#define AM335X_BACKLIGHT_DEFAULT_BRIGHTNESS    100
#define AM335X_PWM_PERIOD_NANO_SECONDS        (5000 * 10)

static struct platform_pwm_backlight_data am335x_backlight_data0 = {
	.pwm_id         = "ecap.0",
	.ch             = -1,
	.lth_brightness	= 21,
	.max_brightness = AM335X_BACKLIGHT_MAX_BRIGHTNESS,
	.dft_brightness = AM335X_BACKLIGHT_DEFAULT_BRIGHTNESS,
	.pwm_period_ns  = AM335X_PWM_PERIOD_NANO_SECONDS,
};

static struct platform_pwm_backlight_data am335x_backlight_data2 = {
	.pwm_id         = "ecap.2",
	.ch             = -1,
	.lth_brightness	= 21,
	.max_brightness = AM335X_BACKLIGHT_MAX_BRIGHTNESS,
	.dft_brightness = AM335X_BACKLIGHT_DEFAULT_BRIGHTNESS,
	.pwm_period_ns  = AM335X_PWM_PERIOD_NANO_SECONDS,
};

static struct lcd_ctrl_config lcd_cfg = {
	&disp_panel,
	.ac_bias		= 255,
	.ac_bias_intrpt		= 0,
	.dma_burst_sz		= 16,
	.bpp			= 32,
	.fdd			= 0x80,
	.tft_alt_mode		= 0,
	.stn_565_mode		= 0,
	.mono_8bit_mode		= 0,
	.invert_line_clock	= 1,
	.invert_frm_clock	= 1,
	.sync_edge		= 0,
	.sync_ctrl		= 1,
	.raster_order		= 0,
};

struct da8xx_lcdc_platform_data TFC_S9700RTWV35TR_01B_pdata = {
	.manu_name		= "ThreeFive",
	.controller_data	= &lcd_cfg,
	.type			= "TFC_S9700RTWV35TR_01B",
};

#ifdef CONFIG_MACH_ADVANTECH
struct da8xx_lcdc_platform_data AUO_G185XW01_V1_pdata = {
	.manu_name		= "AUO",
	.controller_data	= &lcd_cfg,
	.type			= "AUO_G185XW01_V1",
};

struct da8xx_lcdc_platform_data INNOLUX_G150XGE_L04_pdata = {
	.manu_name		= "INNOLUX",
	.controller_data	= &lcd_cfg,
	.type			= "INNOLUX_G150XGE_L04",
};
#ifdef CONFIG_MACH_AM335X_ROM3310
struct da8xx_lcdc_platform_data INNOLUX_VGA_800x600_pdata = {
	.manu_name		= "INNOLUX",
	.controller_data	= &lcd_cfg,
	.type			= "INNOLUX_VGA_800x600@60",
};
struct da8xx_lcdc_platform_data INNOLUX_VGA_1024x768_pdata = {
	.manu_name		= "INNOLUX",
	.controller_data	= &lcd_cfg,
	.type			= "INNOLUX_VGA_1024x768@60",
};
struct da8xx_lcdc_platform_data INNOLUX_VGA_1360x768_pdata = {
	.manu_name		= "INNOLUX",
	.controller_data	= &lcd_cfg,
	.type			= "INNOLUX_VGA_1360x768@60",
};
#endif
#endif

struct da8xx_lcdc_platform_data  NHD_480272MF_ATXI_pdata = {
	.manu_name              = "NHD",
	.controller_data        = &lcd_cfg,
	.type                   = "NHD-4.3-ATXI#-T-1",
};

#include "common.h"

#ifdef CONFIG_MACH_AM335X_ROM3310

static u8 am335x_iis_serializer_direction0[] = {
	RX_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	TX_MODE,
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
};

static struct snd_platform_data am335x_evm_snd_data0 = {
	.tx_dma_offset	= 0x46000000,	/* McASP0 */
	.rx_dma_offset	= 0x46000000,
	.op_mode	= DAVINCI_MCASP_IIS_MODE,
	.num_serializer	= ARRAY_SIZE(am335x_iis_serializer_direction0),
	.tdm_slots	= 2,
	.serial_dir	= am335x_iis_serializer_direction0,
	.asp_chan_q	= EVENTQ_2,
	.version	= MCASP_VERSION_3,
	.txnumevt	= 32,
	.rxnumevt	= 32,
	.get_context_loss_count	=
			omap_pm_get_dev_context_loss_count,
};

static u8 am335x_evm_sk_iis_serializer_direction0[] = {
	INACTIVE_MODE,	INACTIVE_MODE,	TX_MODE,	INACTIVE_MODE,
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
};

static struct snd_platform_data am335x_evm_sk_snd_data0 = {
	.tx_dma_offset	= 0x46000000,	/* McASP0 */
	/*.rx_dma_offset	= 0x46000000,*/
	.op_mode	= DAVINCI_MCASP_IIS_MODE,
	.num_serializer	= ARRAY_SIZE(am335x_evm_sk_iis_serializer_direction0),
	.tdm_slots	= 2,
	.serial_dir	= am335x_evm_sk_iis_serializer_direction0,
	.asp_chan_q	= EVENTQ_2,
	.version	= MCASP_VERSION_3,
	.txnumevt	= 32,
	.get_context_loss_count	=
			omap_pm_get_dev_context_loss_count,
};
#endif

static struct omap2_hsmmc_info am335x_mmc[] __initdata = {
	{
		.mmc            = 1,
		.caps           = MMC_CAP_4_BIT_DATA,
#ifdef CONFIG_MACH_ADVANTECH
		.gpio_cd        = EMMC0_CD_PIN,
		.gpio_wp        = EMMC0_WP_PIN,
#else
		.gpio_cd        = GPIO_TO_PIN(0, 6),
		.gpio_wp        = GPIO_TO_PIN(3, 18),
#endif
		.ocr_mask       = MMC_VDD_32_33 | MMC_VDD_33_34, /* 3V3 */
	},
	{
		.mmc            = 0,	/* will be set at runtime */
	},
	{
		.mmc            = 0,	/* will be set at runtime */
	},
	{}      /* Terminator */
};

#ifdef CONFIG_OMAP_MUX
static struct omap_board_mux board_mux[] __initdata = {
	/*
	 * Setting SYSBOOT[5] should set xdma_event_intr0 pin to mode 3 thereby
	 * allowing clkout1 to be available on xdma_event_intr0.
	 * However, on some boards (like EVM-SK), SYSBOOT[5] isn't properly
	 * latched.
	 * To be extra cautious, setup the pin-mux manually.
	 * If any modules/usecase requries it in different mode, then subsequent
	 * module init call will change the mux accordingly.
	 */
	AM33XX_MUX(XDMA_EVENT_INTR0, OMAP_MUX_MODE3 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(I2C0_SDA, OMAP_MUX_MODE0 | AM33XX_SLEWCTRL_SLOW |
			AM33XX_INPUT_EN | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(I2C0_SCL, OMAP_MUX_MODE0 | AM33XX_SLEWCTRL_SLOW |
			AM33XX_INPUT_EN | AM33XX_PIN_OUTPUT),
	{ .reg_offset = OMAP_MUX_TERMINATOR },
};
#else
#define	board_mux	NULL
#endif

struct evm_dev_cfg {
	void (*device_init)(int evm_id, int profile);

/*
* If the device is required on both baseboard & daughter board (ex i2c),
* specify DEV_ON_BASEBOARD
*/
#define DEV_ON_BASEBOARD	0
#define DEV_ON_DGHTR_BRD	1
	u32 device_on;

	u32 profile;	/* Profiles (0-7) in which the module is present */
};

static u32 am335x_evm_id;
static struct omap_board_config_kernel am335x_evm_config[] __initdata = {
};

/*
* EVM Config held in On-Board eeprom device.
*
* Header Format
*
*  Name			Size	Contents
*			(Bytes)
*-------------------------------------------------------------
*  Header		4	0xAA, 0x55, 0x33, 0xEE
*
*  Board Name		8	Name for board in ASCII.
*				Example "A33515BB" = "AM335x 15x15 Base Board"
*
*  Version		4	Hardware version code for board	in ASCII.
*				"1.0A" = rev.01.0A
*
*  Serial Number	12	Serial number of the board. This is a 12
*				character string which is WWYY4P16nnnn, where
*				WW = 2 digit week of the year of production
*				YY = 2 digit year of production
*				nnnn = incrementing board number
*
*  Configuration option	32	Codes(TBD) to show the configuration
*				setup on this board.
*
*  Available		32720	Available space for other non-volatile data.
*/
struct am335x_evm_eeprom_config {
	u32	header;
	u8	name[8];
	char	version[4];
	u8	serial[12];
	u8	opt[32];
};

/*
* EVM Config held in daughter board eeprom device.
*
* Header Format
*
*  Name			Size		Contents
*			(Bytes)
*-------------------------------------------------------------
*  Header		4	0xAA, 0x55, 0x33, 0xEE
*
*  Board Name		8	Name for board in ASCII.
*				example "A335GPBD" = "AM335x
*				General Purpose Daughterboard"
*
*  Version		4	Hardware version code for board in
*				in ASCII. "1.0A" = rev.01.0A
*  Serial Number	12	Serial number of the board. This is a 12
*				character string which is: WWYY4P13nnnn, where
*				WW = 2 digit week of the year of production
*				YY = 2 digit year of production
*				nnnn = incrementing board number
*  Configuration Option	32	Codes to show the configuration
*				setup on this board.
*  CPLD Version	8		CPLD code version for board in ASCII
*				"CPLD1.0A" = rev. 01.0A of the CPLD
*  Available	32700		Available space for other non-volatile
*				codes/data
*/

struct am335x_eeprom_config1 {
	u32	header;
	u8	name[8];
	char	version[4];
	u8	serial[12];
	u8	opt[32];
	u8	cpld_ver[8];
};

static struct am335x_evm_eeprom_config config;
static struct am335x_eeprom_config1 config1;
static bool daughter_brd_detected;

#define EEPROM_MAC_ADDRESS_OFFSET	60 /* 4+8+4+12+32 */
#define EEPROM_NO_OF_MAC_ADDR		3
static char am335x_mac_addr[EEPROM_NO_OF_MAC_ADDR][ETH_ALEN];

#define AM335X_EEPROM_HEADER		0xEE3355AA

static int am33xx_evmid = -EINVAL;

/*
* am335x_evm_set_id - set up board evmid
* @evmid - evm id which needs to be configured
*
* This function is called to configure board evm id.
*/
void am335x_evm_set_id(unsigned int evmid)
{
	am33xx_evmid = evmid;
	return;
}

/*
* am335x_evm_get_id - returns Board Type (EVM/BB/EVM-SK ...)
*
* Note:
*	returns -EINVAL if Board detection hasn't happened yet.
*/
int am335x_evm_get_id(void)
{
	return am33xx_evmid;
}
EXPORT_SYMBOL(am335x_evm_get_id);

/* current profile if exists else PROFILE_0 on error */
static u32 am335x_get_profile_selection(void)
{
	int val = 0;

	return val & 0x7;
}

/*
* @pin_mux - single module pin-mux structure which defines pin-mux
*			details for all its pins.
*/
static void setup_pin_mux(struct pinmux_config *pin_mux)
{
	int i;

	for (i = 0; pin_mux->string_name != NULL; pin_mux++)
		omap_mux_init_signal(pin_mux->string_name, pin_mux->val);

}

/*
* @evm_id - evm id which needs to be configured
* @dev_cfg - single evm structure which includes
*				all module inits, pin-mux defines
* @profile - if present, else PROFILE_NONE
* @dghtr_brd_flg - Whether Daughter board is present or not
*/
static void _configure_device(int evm_id, struct evm_dev_cfg *dev_cfg,
	int profile)
{
	int i;

	am335x_evm_set_id(evm_id);

	/*
	* Only General Purpose & Industrial Auto Motro Control
	* EVM has profiles. So check if this evm has profile.
	* If not, ignore the profile comparison
	*/

	/*
	* If the device is on baseboard, directly configure it. Else (device on
	* Daughter board), check if the daughter card is detected.
	*/
	if (profile == PROFILE_NONE) {
		for (i = 0; dev_cfg->device_init != NULL; dev_cfg++) {
			if (dev_cfg->device_on == DEV_ON_BASEBOARD)
				dev_cfg->device_init(evm_id, profile);
			else if (daughter_brd_detected == true)
				dev_cfg->device_init(evm_id, profile);
		}
	} else {
		for (i = 0; dev_cfg->device_init != NULL; dev_cfg++) {
			if (dev_cfg->profile & profile) {
				if (dev_cfg->device_on == DEV_ON_BASEBOARD)
					dev_cfg->device_init(evm_id, profile);
				else if (daughter_brd_detected == true)
					dev_cfg->device_init(evm_id, profile);
			}
		}
	}
}

#define AM335XEVM_WLAN_PMENA_GPIO	GPIO_TO_PIN(1, 30)
#define AM335XEVM_WLAN_IRQ_GPIO		GPIO_TO_PIN(3, 17)
#define AM335XEVM_SK_WLAN_IRQ_GPIO      GPIO_TO_PIN(0, 31)

#ifndef CONFIG_MACH_AM335X_WISE3320
struct wl12xx_platform_data am335xevm_wlan_data = {
	.irq = OMAP_GPIO_IRQ(AM335XEVM_WLAN_IRQ_GPIO),
	.board_ref_clock = WL12XX_REFCLOCK_38_XTAL, /* 38.4Mhz */
	.bt_enable_gpio = GPIO_TO_PIN(3, 21),
	.wlan_enable_gpio = GPIO_TO_PIN(1, 16),
};
#endif
static bool backlight_enable;

#ifdef CONFIG_MACH_ADVANTECH
#ifdef CONFIG_MACH_AM335X_ROM3310
int panel_type = 7;
#else
int panel_type = 2;
#endif
#endif

static void enable_ecap2(int evm_id, int profile)
{
	backlight_enable = true;
#ifndef CONFIG_MACH_AM335X_WISE3320
	setup_pin_mux(ecap2_pin_mux);
#endif
}

/* Setup pwm-backlight */
#ifdef CONFIG_MACH_ADVANTECH
static struct platform_device am335x_backlight = {
	.name           = "pwm-backlight",
	.id             = -1,
	.dev		= {
		.platform_data = &am335x_backlight_data2,
	},
};
#else
static struct platform_device am335x_backlight = {
	.name           = "pwm-backlight",
	.id             = -1,
	.dev		= {
		.platform_data = &am335x_backlight_data0,
	},
};
#endif

static struct pwmss_platform_data  pwm_pdata[3] = {
	{
		.version = PWM_VERSION_1,
	},
	{
		.version = PWM_VERSION_1,
	},
	{
		.version = PWM_VERSION_1,
	},
};

static int __init backlight_init(void)
{
	int status = 0;

	if (backlight_enable) {
		int ecap_index = 0;

		switch (am335x_evm_get_id()) {
		case GEN_PURP_EVM:
		case GEN_PURP_DDR3_EVM:
#ifdef CONFIG_MACH_ADVANTECH
			ecap_index = 2;
#else
			ecap_index = 0;
#endif
			break;
		case EVM_SK:
			/*
			 * Invert polarity of PWM wave from ECAP to handle
			 * backlight intensity to pwm brightness
			 */
			ecap_index = 2;
			pwm_pdata[ecap_index].chan_attrib[0].inverse_pol = true;
			am335x_backlight.dev.platform_data =
				&am335x_backlight_data2;
			break;
		default:
			pr_err("%s: Error on attempting to enable backlight,"
				" not supported\n", __func__);
			return -EINVAL;
		}

		am33xx_register_ecap(ecap_index, &pwm_pdata[ecap_index]);
		platform_device_register(&am335x_backlight);
	}
	return status;
}
late_initcall(backlight_init);

static int __init conf_disp_pll(int rate)
{
	struct clk *disp_pll;
	int ret = -EINVAL;

	disp_pll = clk_get(NULL, "dpll_disp_ck");
	if (IS_ERR(disp_pll)) {
		pr_err("Cannot clk_get disp_pll\n");
		goto out;
	}

	ret = clk_set_rate(disp_pll, rate);
	clk_put(disp_pll);
out:
	return ret;
}

#ifdef CONFIG_MACH_ADVANTECH
static int __init panel_setup(char * panel)
{
	if(!strcmp(panel, "Sharp_LCD035Q3DG01"))
	{
		panel_type = 0;
	}else if(!strcmp(panel, "Sharp_LK043T1DG01"))
	{
		panel_type = 1;
	}else if(!strcmp(panel, "TFC_S9700RTWV35TR_01B"))
	{
		panel_type = 2;
	}else if(!strcmp(panel, "NHD-4.3-ATXI#-T-1"))
	{
		panel_type = 3;
	}else if(!strcmp(panel, "AUO_G185XW01_V1"))
	{
		panel_type = 4;
 	}else if(!strcmp(panel, "INNOLUX_G150XGE_L04"))
	{
		panel_type = 5;
	}
#ifdef CONFIG_MACH_AM335X_ROM3310
	else if(!strcmp(panel, "VGA_800x600@60"))
	{
		panel_type = 6;
	}
	else if(!strcmp(panel, "VGA_1024x768@60"))
	{
		panel_type = 7;
	}
	else if(!strcmp(panel, "VGA_1360x768@60"))
	{
		panel_type = 8;
	}
#endif
        else
	{
#ifdef CONFIG_MACH_AM335X_ROM3310
		panel_type = 7;
#else
		panel_type = 2;
#endif
        }
        return 1;
}

#ifdef CONFIG_MACH_AM335X_ROM3310
__setup("video_mode=", panel_setup);
#else
__setup("lvds_panel=", panel_setup);
#endif

/*Form parm get MAC0 & MAC1*/
/*Add Ryan.xin*/
static int parse_mac_addr(char *mac_addr, int index)
{
	char *ptr, *p = mac_addr;
	unsigned long tmp;
	int i = 0, ret = 0;

	while (p && (*p) && i < ETH_ALEN) {
		ptr = strchr(p, ':');
		if (ptr)
			*ptr++ = '\0';

		if (strlen(p)) {
			ret = strict_strtoul(p, 16, &tmp);
			if (ret < 0 || tmp > 0xff)
				break;
			am335x_mac_addr[index][i++] = tmp;
		}
		p = ptr;
	}

	return 0;
}

static void __init mac_address_setup(char *macaddress)
{
 	char buf[ETH_ALEN * ETH_ALEN] = {0};
        char *ptr = NULL;

        if(NULL == macaddress)
	{
		memset(am335x_mac_addr, 0x0, sizeof(am335x_mac_addr));
		return;
	}
#ifdef CONFIG_MACH_AM335X_ROM3310
	strncpy(buf, macaddress, 17);
	parse_mac_addr(buf, 0);
#endif
#ifdef CONFIG_MACH_AM335X_RSB4220
	ptr = strchr(macaddress, ',');
        strncpy(buf, macaddress, strlen(ptr) - 1);
	parse_mac_addr(buf, 0);
        memset(buf, 0x0, sizeof(buf));
        strncpy(buf, ptr + 1, strlen(ptr) - 1);
	parse_mac_addr(buf, 1);
#endif
}

__setup("mac_addr=", mac_address_setup);

#endif

static void lcdc_init(int evm_id, int profile)
{
	struct da8xx_lcdc_platform_data *lcdc_pdata;
	setup_pin_mux(lcdc_pin_mux);

#ifdef CONFIG_MACH_AM335X_ROM3310
	unsigned long disp_clock = 0;
	if ( panel_type == 6 )
		disp_clock = 280000000;
	else if(panel_type == 7)
		disp_clock = 260000000;
	else if(panel_type == 8)
		disp_clock = 256500000;
	else
		disp_clock = 260000000;
	if (conf_disp_pll(disp_clock)) {
#else
	if (conf_disp_pll(300000000)) {
#endif
		pr_info("Failed configure display PLL, not attempting to"
				"register LCDC\n");
		return;
	}
	switch (evm_id) {
	case GEN_PURP_EVM:
	case GEN_PURP_DDR3_EVM:
#ifdef CONFIG_MACH_ADVANTECH
		if ( panel_type == 0 )
			lcdc_pdata = &TFC_S9700RTWV35TR_01B_pdata;
		else if ( panel_type == 1 )
			lcdc_pdata = &TFC_S9700RTWV35TR_01B_pdata;
		else if ( panel_type == 2 )
			lcdc_pdata = &TFC_S9700RTWV35TR_01B_pdata;
		else if ( panel_type == 3 )
			lcdc_pdata = &NHD_480272MF_ATXI_pdata;
		else if ( panel_type == 4 )
			lcdc_pdata = &AUO_G185XW01_V1_pdata;
		else if ( panel_type == 5 )
			lcdc_pdata = &INNOLUX_G150XGE_L04_pdata;
#ifdef CONFIG_MACH_AM335X_ROM3310
		else if ( panel_type == 6 )
			lcdc_pdata = &INNOLUX_VGA_800x600_pdata;
		else if ( panel_type == 7 )
			lcdc_pdata = &INNOLUX_VGA_1024x768_pdata;
		else if ( panel_type == 8 )
			lcdc_pdata = &INNOLUX_VGA_1360x768_pdata;
#endif
#else
		lcdc_pdata = &TFC_S9700RTWV35TR_01B_pdata;
#endif
		break;
	case EVM_SK:
		lcdc_pdata = &NHD_480272MF_ATXI_pdata;
		break;
	default:
		pr_err("LCDC not supported on this evm (%d)\n",evm_id);
		return;
	}

	lcdc_pdata->get_context_loss_count = omap_pm_get_dev_context_loss_count;

	if (am33xx_register_lcdc(lcdc_pdata))
		pr_info("Failed to register LCDC device\n");

	return;
}

static void rgmii1_init(int evm_id, int profile)
{
	setup_pin_mux(rgmii1_pin_mux);
	return;
}

static void rgmii2_init(int evm_id, int profile)
{
	setup_pin_mux(rgmii2_pin_mux);
	return;
}

static void mii1_init(int evm_id, int profile)
{
	setup_pin_mux(mii1_pin_mux);
	return;
}

static void rmii1_init(int evm_id, int profile)
{
	setup_pin_mux(rmii1_pin_mux);
	return;
}

static void usb0_init(int evm_id, int profile)
{
	setup_pin_mux(usb0_pin_mux);
	return;
}

static void usb1_init(int evm_id, int profile)
{
	setup_pin_mux(usb1_pin_mux);
	return;
}

/* setup uart3 */
static void uart3_init(int evm_id, int profile)
{
	setup_pin_mux(uart3_pin_mux);
	return;
}

/* setup uart2 */
static void uart2_init(int evm_id, int profile)
{
	setup_pin_mux(uart2_pin_mux);
	return;
}

#ifdef CONFIG_MACH_ADVANTECH
/*uart4 */
static void uart4_init(int evm_id, int profile)
{
        setup_pin_mux(uart4_pin_mux);
        return;
}
 /*setup uart5     yuecheng */ 
static void uart5_init(int evm_id, int profile)
{
        setup_pin_mux(uart5_pin_mux);
        return;
}
#endif

/*
 * gpio0_7 was driven HIGH in u-boot before DDR configuration
 *
 * setup gpio0_7 for EVM-SK 1.2
 */
static void gpio_ddr_vtt_enb_init(int evm_id, int profile)
{
	setup_pin_mux(gpio_ddr_vtt_enb_pin_mux);
	return;
}

#if defined(CONFIG_MACH_AM335X_ROM3310) || defined(CONFIG_MACH_AM335X_RSB4220)
/*
 * gpio2_5 was driven WDT MSP430
 *
 * setup gpio2_5 for EVM-SK 1.2
 */
static void gpio_wdt_init(int evm_id, int profile)
{
	setup_pin_mux(wdt_pin_mux);
#ifdef CONFIG_MACH_AM335X_RSB4220
	gpio_request(GPIO_TO_PIN(2,4), "GPIO_WDI");
	gpio_direction_output(GPIO_TO_PIN(2,4), 0);
	gpio_export(GPIO_TO_PIN(2,4), 1);
	gpio_request(GPIO_TO_PIN(2,0), "WDT_EN");
	gpio_direction_output(GPIO_TO_PIN(2,0), 0);
	gpio_export(GPIO_TO_PIN(2,0), 1);
#endif
#ifdef CONFIG_MACH_AM335X_ROM3310
	gpio_request(GPIO_TO_PIN(2,5), "GPIO_WDI");
	gpio_direction_output(GPIO_TO_PIN(2,5), 0);
	gpio_export(GPIO_TO_PIN(2,5), 1);
	gpio_request(GPIO_TO_PIN(3,4), "WDT_EN");
	gpio_direction_output(GPIO_TO_PIN(3,4), 0);
	gpio_export(GPIO_TO_PIN(3,4), 1);
#endif
	return;
}
#endif

/* setup haptics */
#define HAPTICS_MAX_FREQ 250
static void haptics_init(int evm_id, int profile)
{
	setup_pin_mux(haptics_pin_mux);
	pwm_pdata[2].chan_attrib[1].max_freq = HAPTICS_MAX_FREQ;
	am33xx_register_ehrpwm(2, &pwm_pdata[2]);
}

/* NAND partition information */
static struct mtd_partition am335x_nand_partitions[] = {
/* All the partition sizes are listed in terms of NAND block size */
	{
		.name           = "SPL",
		.offset         = 0,			/* Offset = 0x0 */
		.size           = SZ_128K,
	},
	{
		.name           = "SPL.backup1",
		.offset         = MTDPART_OFS_APPEND,	/* Offset = 0x20000 */
		.size           = SZ_128K,
	},
	{
		.name           = "SPL.backup2",
		.offset         = MTDPART_OFS_APPEND,	/* Offset = 0x40000 */
		.size           = SZ_128K,
	},
	{
		.name           = "SPL.backup3",
		.offset         = MTDPART_OFS_APPEND,	/* Offset = 0x60000 */
		.size           = SZ_128K,
	},
	{
		.name           = "U-Boot",
		.offset         = MTDPART_OFS_APPEND,   /* Offset = 0x80000 */
		.size           = 15 * SZ_128K,
	},
	{
		.name           = "U-Boot Env",
		.offset         = MTDPART_OFS_APPEND,   /* Offset = 0x260000 */
		.size           = 1 * SZ_128K,
	},
	{
		.name           = "Kernel",
		.offset         = MTDPART_OFS_APPEND,   /* Offset = 0x280000 */
		.size           = 40 * SZ_128K,
	},
	{
		.name           = "File System",
		.offset         = MTDPART_OFS_APPEND,   /* Offset = 0x780000 */
		.size           = MTDPART_SIZ_FULL,
	},
};

/* SPI 0/1 Platform Data */
/* SPI flash information */
static struct mtd_partition am335x_spi_partitions[] = {
#ifdef CONFIG_MACH_ADVANTECH
	{
		.name       = "SPL",
		.offset     = 0,			/* Offset = 0x0 */
		.size       = (4 << 20),
	}
#else
	/* All the partition sizes are listed in terms of erase size */
	{
		.name       = "SPL",
		.offset     = 0,			/* Offset = 0x0 */
		.size       = SZ_128K,
	},
	{
		.name       = "U-Boot",
		.offset     = MTDPART_OFS_APPEND,	/* Offset = 0x20000 */
		.size       = (3 * SZ_128K) - SZ_4K,
	},
	{
		.name       = "U-Boot Env",
		.offset     = MTDPART_OFS_APPEND,	/* Offset = 0x7F000 */
		.size       = SZ_4K,
	},
	{
		.name       = "Kernel",
		.offset     = MTDPART_OFS_APPEND,	/* Offset = 0x80000 */
		.size       = 866 * SZ_4K,		/* size = 0x362000 */
	},
	{
		.name       = "File System",
		.offset     = MTDPART_OFS_APPEND,	/* Offset = 0x3E2000 */
		.size       = MTDPART_SIZ_FULL,		/* size ~= 4.1 MiB */
	}
#endif
};

#ifdef CONFIG_MACH_ADVANTECH  //-------------------------------------------------
/* pwrch, add for Winbond W25Q32 
{ "w25q32", INFO(0xef4016, 0, 64 * 1024,  64, SECT_4K) },
{ "w25q64", INFO(0xef4017, 0, 64 * 1024, 128, SECT_4K) }
*/
static const struct flash_platform_data am335x_spi_flash = {
	.type      = "w25q32",
	.name      = "spi_flash",
	.parts     = am335x_spi_partitions,
	.nr_parts  = ARRAY_SIZE(am335x_spi_partitions),
};
#else //-----------------------------------------

static const struct flash_platform_data am335x_spi_flash = {
	.type      = "w25q64",
	.name      = "spi_flash",
	.parts     = am335x_spi_partitions,
	.nr_parts  = ARRAY_SIZE(am335x_spi_partitions),
};
#endif //----------------------------------

/*
 * SPI Flash works at 80Mhz however SPI Controller works at 48MHz.
 * So setup Max speed to be less than that of Controller speed
 */
#ifdef CONFIG_MACH_ADVANTECH
static struct spi_board_info am335x_spi0_slave_info[] = {
	{
		.modalias      = "m25p80",
		.platform_data = &am335x_spi_flash,
		.irq           = -1,
		.max_speed_hz  = 12000000,//pwrch, make it 12M, buggy, will halt by mtd_debug write //orig=24000000,
		.bus_num       = 1,
		.chip_select   = 0,
	},
};
#else
static struct spi_board_info am335x_spi0_slave_info[] = {
	{
		.modalias      = "m25p80",
		.platform_data = &am335x_spi_flash,
		.irq           = -1,
		.max_speed_hz  = 24000000,
		.bus_num       = 1,
		.chip_select   = 0,
	},
};
#endif

#ifdef CONFIG_MACH_AM335X_ROM3310_SPI1
static struct mtd_partition am335x_spi1_partitions[] = {
	/* All the partition sizes are listed in terms of erase size */
	{
		.name       = "SPI1",
		.offset     = 0,			/* Offset = 0x0 */
		.size       = 0x00400000,
	},
};

static const struct flash_platform_data am335x_spi1_flash = {
	.type      = "n25q",
	.name      = "spi_flash",
	.parts     = am335x_spi1_partitions,
	.nr_parts  = ARRAY_SIZE(am335x_spi1_partitions),
};

static struct spi_board_info am335x_spi1_slave_info[] = {
	{
		.modalias      = "m25p80",
		.platform_data = &am335x_spi1_flash,
		.irq           = -1,
		.max_speed_hz  = 20000000,
		.bus_num       = 2,
		.chip_select   = 0,
	},
};
#endif

static void i2c1_init(int evm_id, int profile)
{
	setup_pin_mux(i2c1_pin_mux);
	omap_register_i2c_bus(2, 100, am335x_i2c1_boardinfo,
			ARRAY_SIZE(am335x_i2c1_boardinfo));
	return;
}

static void i2c2_init(int evm_id, int profile)
{
	setup_pin_mux(i2c2_pin_mux);
	omap_register_i2c_bus(3, 100, am335x_i2c2_boardinfo,
			ARRAY_SIZE(am335x_i2c2_boardinfo));
	return;
}

#ifdef CONFIG_MACH_AM335X_ROM3310
/* Setup McASP 0 */
static void mcasp0_init(int evm_id, int profile)
{
	/* Configure McASP */
	setup_pin_mux(mcasp0_pin_mux);
	switch (evm_id) {
	case EVM_SK:
		am335x_register_mcasp(&am335x_evm_sk_snd_data0, 0);
		break;
	default:
		am335x_register_mcasp(&am335x_evm_snd_data0, 0);
	}

	return;
}
#endif

#ifdef CONFIG_MACH_AM335X_WISE3320
static void rf_board_gpio_init(int evm_id, int profile)
{
	printk("RF-board pin init\n");
	setup_pin_mux(rf_board_pin_mux);
	int n = 0;

	/* GPIO1_0 ~ GPIO1_7*/
	for (n = 0; n < 8; ++n)
	{
		gpio_request(GPIO_TO_PIN(1, n), "RF_BOARD");
		gpio_direction_output(GPIO_TO_PIN(1, n), 0);
		gpio_export(GPIO_TO_PIN(1, n), 1);
	}
	gpio_direction_output(GPIO_TO_PIN(1, 1), 1);
	gpio_direction_output(GPIO_TO_PIN(1, 5), 1);

	gpio_direction_output(GPIO_TO_PIN(1, 1), 0);
	gpio_direction_output(GPIO_TO_PIN(1, 5), 0);
}

#ifdef CONFIG_MACH_AM335X_WISE3320
/* 
* Add by Qing for rs485/232 switch 
*/
static void rs485_gpio_init(int evm_id, int profile)
{
	setup_pin_mux(rs485_pin_mux);
	gpio_request(GPIO_TO_PIN(1, 13), "WISE3320 Reset");
	gpio_direction_input(GPIO_TO_PIN(1, 13));
}
/*End*/
#endif

static struct resource advreset_resource[] = {
	{
		.start	= OMAP_GPIO_IRQ(GPIO_TO_PIN(3, 17)), /* set later */
		.flags	= IORESOURCE_IRQ,
	},
};

static struct platform_device advreset_device = {
	.name		= "advreset-gpio",
	.id		= -1,
	.resource	= advreset_resource,
	.num_resources	= ARRAY_SIZE(advreset_resource),
	.dev		= {
		.platform_data = &advreset_time,
	},
};

static void reset_gpio_init(int evm_id, int profile)
{
	setup_pin_mux(reset_pin_mux);
	gpio_request(GPIO_TO_PIN(3, 17), "WISE3320 Reset");
	gpio_direction_input(GPIO_TO_PIN(3, 17));
	irq_set_irq_type(gpio_to_irq(GPIO_TO_PIN(3, 17)), IRQ_TYPE_EDGE_BOTH);
	platform_device_register(&advreset_device);
}
#endif
static void mmc1_emmc_init(int evm_id, int profile)
{
	setup_pin_mux(mmc1_common_pin_mux);
	setup_pin_mux(mmc1_dat4_7_pin_mux);

	am335x_mmc[1].mmc = 2;
	am335x_mmc[1].caps = MMC_CAP_8_BIT_DATA;
	am335x_mmc[1].gpio_cd = -EINVAL;
	am335x_mmc[1].gpio_wp = -EINVAL;
	am335x_mmc[1].ocr_mask = MMC_VDD_32_33 | MMC_VDD_33_34; /* 3V3 */

#ifdef CONFIG_MACH_ADVANTECH
	am335x_mmc[1].nonremovable = true;
#endif
	/* mmc will be initialized when mmc0_init is called */
	return;
}

static void uart1_wl12xx_init(int evm_id, int profile)
{
	setup_pin_mux(uart1_wl12xx_pin_mux);
}

static void d_can_init(int evm_id, int profile)
{
	switch (evm_id) {
	case IND_AUT_MTR_EVM:
		if ((profile == PROFILE_0) || (profile == PROFILE_1)) {
			setup_pin_mux(d_can_ia_pin_mux);
			/* Instance Zero */
			am33xx_d_can_init(0);
		}
		break;
	case GEN_PURP_EVM:
	case GEN_PURP_DDR3_EVM:
#ifdef CONFIG_MACH_ADVANTECH
		if (profile == PROFILE_0) {
#else
		if (profile == PROFILE_1) {
#endif
#ifdef CONFIG_MACH_AM335X_ROM3310
			setup_pin_mux(d_can_ia_pin_mux);
			am33xx_d_can_init(0);
#endif
			setup_pin_mux(d_can_gp_pin_mux);
			/* Instance One */
			am33xx_d_can_init(1);
		}
		break;
	default:
		break;
	}
}


static void mmc0_init(int evm_id, int profile)
{
	switch (evm_id) {
	case BEAGLE_BONE_A3:
	case BEAGLE_BONE_OLD:
	case EVM_SK:
		setup_pin_mux(mmc0_common_pin_mux);
		setup_pin_mux(mmc0_cd_only_pin_mux);
		break;
	default:
		setup_pin_mux(mmc0_common_pin_mux);
		setup_pin_mux(mmc0_cd_only_pin_mux);
		setup_pin_mux(mmc0_wp_only_pin_mux);
		break;
	}

	omap2_hsmmc_init(am335x_mmc);
	return;
}

static void mmc0_no_cd_init(int evm_id, int profile)
{
	setup_pin_mux(mmc0_common_pin_mux);
	setup_pin_mux(mmc0_wp_only_pin_mux);

	omap2_hsmmc_init(am335x_mmc);
	return;
}

/* setup spi0 */
static void spi0_init(int evm_id, int profile)
{
	setup_pin_mux(spi0_pin_mux);
	spi_register_board_info(am335x_spi0_slave_info,
			ARRAY_SIZE(am335x_spi0_slave_info));
	return;
}

#ifdef CONFIG_MACH_AM335X_ROM3310_SPI1
static void spi1_init(int evm_id, int profile)
{
	setup_pin_mux(spi1_pin_mux);
	spi_register_board_info(am335x_spi1_slave_info,
			ARRAY_SIZE(am335x_spi1_slave_info));
	return;
}
#endif

#ifdef CONFIG_MACH_AM335X_ROM3310
static void gpio_keys_init(int evm_id, int profile)
{
	int err;

	setup_pin_mux(gpio_keys_pin_mux);
	err = platform_device_register(&am335x_evm_gpio_keys);
	if (err)
		pr_err("failed to register gpio key device\n");
}
#endif

static struct omap_rtc_pdata am335x_rtc_info = {
	.pm_off		= false,
	.wakeup_capable	= 0,
};

static void am335x_rtc_init(int evm_id, int profile)
{
	void __iomem *base;
	struct clk *clk;
	struct omap_hwmod *oh;
	struct platform_device *pdev;
	char *dev_name = "am33xx-rtc";

	clk = clk_get(NULL, "rtc_fck");
	if (IS_ERR(clk)) {
		pr_err("rtc : Failed to get RTC clock\n");
		return;
	}

	if (clk_enable(clk)) {
		pr_err("rtc: Clock Enable Failed\n");
		return;
	}

	base = ioremap(AM33XX_RTC_BASE, SZ_4K);

	if (WARN_ON(!base))
		return;

	/* Unlock the rtc's registers */
	writel(0x83e70b13, base + 0x6c);
	writel(0x95a4f1e0, base + 0x70);

	/*
	 * Enable the 32K OSc
	 * TODO: Need a better way to handle this
	 * Since we want the clock to be running before mmc init
	 * we need to do it before the rtc probe happens
	 */
	writel(0x48, base + 0x54);

	iounmap(base);

	switch (evm_id) {
	case BEAGLE_BONE_A3:
	case BEAGLE_BONE_OLD:
		am335x_rtc_info.pm_off = true;
		break;
	default:
		break;
	}

	clk_disable(clk);
	clk_put(clk);

	if (omap_rev() >= AM335X_REV_ES2_0)
		am335x_rtc_info.wakeup_capable = 1;

	oh = omap_hwmod_lookup("rtc");
	if (!oh) {
		pr_err("could not look up %s\n", "rtc");
		return;
	}

	pdev = omap_device_build(dev_name, -1, oh, &am335x_rtc_info,
			sizeof(struct omap_rtc_pdata), NULL, 0, 0);
	WARN(IS_ERR(pdev), "Can't build omap_device for %s:%s.\n",
			dev_name, oh->name);
}

static void sgx_init(int evm_id, int profile)
{
	if (omap3_has_sgx()) {
		am33xx_gpu_init();
	}
}

/* General Purpose EVM */
static struct evm_dev_cfg gen_purp_evm_dev_cfg[] = {
#ifdef CONFIG_MACH_ADVANTECH
#else
	{am335x_rtc_init, DEV_ON_BASEBOARD, PROFILE_ALL},
#endif
	{lcdc_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
	{enable_ecap2,  DEV_ON_BASEBOARD, PROFILE_ALL},
#ifdef CONFIG_MACH_AM335X_ROM3310
#else
	{rgmii1_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
#endif
	{rgmii2_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
	{usb0_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
	{usb1_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
#ifndef CONFIG_MACH_ADVANTECH
	{mmc0_init,	DEV_ON_BASEBOARD, (PROFILE_ALL & ~PROFILE_5)},
	{mmc0_no_cd_init,DEV_ON_BASEBOARD, PROFILE_5},
#endif
#ifndef CONFIG_MACH_AM335X_WISE3320
	{mmc1_emmc_init,DEV_ON_BASEBOARD, PROFILE_ALL},
#endif
#ifdef CONFIG_MACH_AM335X_WISE3320
	{rs485_gpio_init,DEV_ON_BASEBOARD, PROFILE_ALL},//Add by Qing
#endif
	{mmc0_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
#ifdef CONFIG_MACH_AM335X_ROM3310
	{mcasp0_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
	{gpio_keys_init,DEV_ON_BASEBOARD, PROFILE_ALL},
#endif
	{spi0_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
#ifdef CONFIG_MACH_AM335X_ROM3310_SPI1
	{spi1_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
#endif
	{uart1_wl12xx_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
#ifndef CONFIG_MACH_AM335X_ROM3310_SPI1
	{uart2_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
#endif
#ifdef CONFIG_MACH_AM335X_ROM3310
#else
	{uart3_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
#endif
	{uart4_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
#ifndef CONFIG_MACH_AM335X_ROM3310_SPI1
	{uart5_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
#endif
#ifndef CONFIG_MACH_AM335X_WISE3320
	{d_can_init,	DEV_ON_BASEBOARD, PROFILE_0},/*PROFILE_ALL & PROFILE_ALL OK*/
#endif
	{sgx_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
#ifdef CONFIG_MACH_AM335X_WISE3320
	{rf_board_gpio_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
	{reset_gpio_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
#endif
#if defined(CONFIG_MACH_AM335X_ROM3310) || defined(CONFIG_MACH_AM335X_RSB4220)
	{gpio_wdt_init,	DEV_ON_BASEBOARD, PROFILE_ALL},
#endif
	{NULL, 0, 0},
};


static int am33xx_evm_tx_clk_dly_phy_fixup(struct phy_device *phydev)
{
	phy_write(phydev, AR8051_PHY_DEBUG_ADDR_REG,
		  AR8051_DEBUG_RGMII_CLK_DLY_REG);
	phy_write(phydev, AR8051_PHY_DEBUG_DATA_REG, AR8051_RGMII_TX_CLK_DLY);

	return 0;
}

#define AM33XX_VDD_CORE_OPP50_UV	1100000
#define AM33XX_OPP120_FREQ		600000000
#define AM33XX_OPPTURBO_FREQ		720000000

#define AM33XX_ES2_0_VDD_CORE_OPP50_UV	950000
#define AM33XX_ES2_0_OPP120_FREQ	720000000
#define AM33XX_ES2_0_OPPTURBO_FREQ	800000000
#define AM33XX_ES2_0_OPPNITRO_FREQ	1000000000

#define AM33XX_ES2_1_VDD_CORE_OPP50_UV	950000
#define AM33XX_ES2_1_OPP120_FREQ	720000000
#define AM33XX_ES2_1_OPPTURBO_FREQ	800000000
#define AM33XX_ES2_1_OPPNITRO_FREQ	1000000000

static void am335x_opp_update(void)
{
	u32 rev;
	int voltage_uv = 0;
	struct device *core_dev, *mpu_dev;
	struct regulator *core_reg;

	core_dev = omap_device_get_by_hwmod_name("l3_main");
	mpu_dev = omap_device_get_by_hwmod_name("mpu");

	if (!mpu_dev || !core_dev) {
		pr_err("%s: Aiee.. no mpu/core devices? %p %p\n", __func__,
		       mpu_dev, core_dev);
		return;
	}

	core_reg = regulator_get(core_dev, "vdd_core");
	if (IS_ERR(core_reg)) {
		pr_err("%s: unable to get core regulator\n", __func__);
		return;
	}

	/*
	 * Ensure physical regulator is present.
	 * (e.g. could be dummy regulator.)
	 */
	voltage_uv = regulator_get_voltage(core_reg);
	if (voltage_uv < 0) {
		pr_err("%s: physical regulator not present for core" \
		       "(%d)\n", __func__, voltage_uv);
		regulator_put(core_reg);
		return;
	}

	pr_debug("%s: core regulator value %d\n", __func__, voltage_uv);
	if (voltage_uv > 0) {
		rev = omap_rev();
		switch (rev) {
		case AM335X_REV_ES1_0:
			if (voltage_uv <= AM33XX_VDD_CORE_OPP50_UV) {
				/*
				 * disable the higher freqs - we dont care about
				 * the results
				 */
				opp_disable(mpu_dev, AM33XX_OPP120_FREQ);
				opp_disable(mpu_dev, AM33XX_OPPTURBO_FREQ);
			}
			break;
		case AM335X_REV_ES2_0:
			if (voltage_uv <= AM33XX_ES2_0_VDD_CORE_OPP50_UV) {
				/*
				 * disable the higher freqs - we dont care about
				 * the results
				 */
				opp_disable(mpu_dev,
					    AM33XX_ES2_0_OPP120_FREQ);
				opp_disable(mpu_dev,
					    AM33XX_ES2_0_OPPTURBO_FREQ);
				opp_disable(mpu_dev,
					    AM33XX_ES2_0_OPPNITRO_FREQ);
			}
			break;
		case AM335X_REV_ES2_1:
		/* FALLTHROUGH */
		default:
			if (voltage_uv <= AM33XX_ES2_1_VDD_CORE_OPP50_UV) {
				/*
				 * disable the higher freqs - we dont care about
				 * the results
				 */
				opp_disable(mpu_dev,
					    AM33XX_ES2_1_OPP120_FREQ);
				opp_disable(mpu_dev,
					    AM33XX_ES2_1_OPPTURBO_FREQ);
				opp_disable(mpu_dev,
					    AM33XX_ES2_1_OPPNITRO_FREQ);
			}
			break;
		}
	}
}

static void setup_general_purpose_evm(void)
{
	u32 prof_sel = am335x_get_profile_selection();
	u32 boardid = GEN_PURP_EVM;

	if (!strncmp("1.5A", config.version, 4))
		boardid = GEN_PURP_DDR3_EVM;

	pr_info("The board is general purpose EVM %sin profile %d\n",
			((boardid == GEN_PURP_DDR3_EVM) ? "with DDR3 " : ""),
			prof_sel);

	_configure_device(boardid, gen_purp_evm_dev_cfg, (1L << prof_sel));

#ifdef CONFIG_MACH_ADVANTECH
#ifdef CONFIG_MACH_AM335X_RSB4220
	am33xx_cpsw_init(AM33XX_CPSW_MODE_RGMII, "0:03", "0:07"); /*RSB4220 DVT PHY ID*/
	/*am33xx_cpsw_init(AM33XX_CPSW_MODE_RGMII, "0:04", "0:05");*/  /* RSB4220 EVT PHY */
#endif
#ifdef CONFIG_MACH_AM335X_ROM3310
	am33xx_cpsw_init(AM33XX_CPSW_MODE_RGMII, NULL, "0:07");
	/*am33xx_cpsw_init(AM33XX_CPSW_MODE_RGMII, NULL, "0:01");*/
#endif
#ifdef CONFIG_MACH_AM335X_WISE3320
	am33xx_cpsw_init(AM33XX_CPSW_MODE_RGMII, "0:03", "0:07");
#endif
#else
	am33xx_cpsw_init(AM33XX_CPSW_MODE_RGMII, NULL, NULL);
#endif //CONFIG_MACH_ADVANTECH
	/* Atheros Tx Clk delay Phy fixup */
	phy_register_fixup_for_uid(AM335X_EVM_PHY_ID, AM335X_EVM_PHY_MASK,
				   am33xx_evm_tx_clk_dly_phy_fixup);
}

static void am335x_evm_setup(struct memory_accessor *mem_acc, void *context)
{
	int ret;
	char tmp[10];

#ifndef CONFIG_MACH_ADVANTECH
	/* 1st get the MAC address from EEPROM */
	ret = mem_acc->read(mem_acc, (char *)&am335x_mac_addr,
		EEPROM_MAC_ADDRESS_OFFSET, sizeof(am335x_mac_addr));

	if (ret != sizeof(am335x_mac_addr)) {
		pr_warning("AM335X: EVM Config read fail: %d\n", ret);
		return;
	}

	/* Fillup global mac id */
	am33xx_cpsw_macidfillup(&am335x_mac_addr[0][0],
				&am335x_mac_addr[1][0]);

	/* get board specific data */
	ret = mem_acc->read(mem_acc, (char *)&config, 0, sizeof(config));
	if (ret != sizeof(config)) {
		pr_err("AM335X EVM config read fail, read %d bytes\n", ret);
		pr_err("This likely means that there either is no/or a failed EEPROM\n");
		goto out;
	}

	if (config.header != AM335X_EEPROM_HEADER) {
		pr_err("AM335X: wrong header 0x%x, expected 0x%x\n",
			config.header, AM335X_EEPROM_HEADER);
		goto out;
	}
#endif

#ifdef CONFIG_MACH_ADVANTECH
	am33xx_cpsw_macidfillup(&am335x_mac_addr[0][0], &am335x_mac_addr[1][0]);
	strcpy(config.name,"A33515BB");
	strcpy(config.version,"1.5A");
	strcpy(config.opt,"SKU#01");
#endif
	if (strncmp("A335", config.name, 4)) {
		pr_err("Board %s\ndoesn't look like an AM335x board\n",
			config.name);
		goto out;
	}

	snprintf(tmp, sizeof(config.name) + 1, "%s", config.name);
	pr_info("Board name: %s\n", tmp);
	snprintf(tmp, sizeof(config.version) + 1, "%s", config.version);
	pr_info("Board version: %s\n", tmp);

	if (!strncmp("A335BONE", config.name, 8)) {
	} else if (!strncmp("A335BNLT", config.name, 8)) {
	} else if (!strncmp("A335X_SK", config.name, 8)) {
	} else {
		/* only 6 characters of options string used for now */
		snprintf(tmp, 7, "%s", config.opt);
		pr_info("SKU: %s\n", tmp);

		if (!strncmp("SKU#01", config.opt, 6))
			setup_general_purpose_evm();
		else
			goto out;
	}

	am335x_opp_update();

	/*
	 * For now, Beaglebone Black uses PG 2.0 that are speed binned and operate
	 * up to 1GHz. So re-enable Turbo and Nitro modes,
	 */
	if (!strncmp("A335BNLT", config.name, 8)) {
		struct device *mpu_dev;

		mpu_dev = omap_device_get_by_hwmod_name("mpu");
		opp_enable(mpu_dev,
			    AM33XX_ES2_0_OPPTURBO_FREQ);
		opp_enable(mpu_dev,
			    AM33XX_ES2_0_OPPNITRO_FREQ);
	}

	/* SmartReflex also requires board information. */
	am33xx_sr_init();

	return;

out:
	/*
	 * If the EEPROM hasn't been programed or an incorrect header
	 * or board name are read then the hardware details are unknown.
	 * Notify the user and call machine_halt to stop the boot process.
	 */
	pr_err("The error message above indicates that there is an issue with\n"
		   "the EEPROM or the EEPROM contents.  After verifying the EEPROM\n"
		   "contents, if any, refer to the %s function in the\n"
		   "%s file to modify the board\n"
		   "initialization code to match the hardware configuration\n",
		   __func__ , __FILE__);
	machine_halt();
}

static struct at24_platform_data am335x_baseboard_eeprom_info = {
#ifdef CONFIG_MACH_AM335X_ROM3310
	.byte_len       = (32*1024) / 8,
#else
	.byte_len       = (256*1024) / 8,
#endif
	.page_size      = 64,
	.flags          = AT24_FLAG_ADDR16,
	.setup          = am335x_evm_setup,
	.context        = (void *)NULL,
};

#ifdef CONFIG_MACH_ADVANTECH
/*Add GPIO PCA953X*/
static int am335x_pca953x_setup(struct i2c_client *client,
                                unsigned gpio_base, unsigned ngpio,
                                void *context)
{
#ifdef CONFIG_MACH_AM335X_RSB4220
	static int am335x_gpio_value[] = {-1, -1, -1, -1, 1, 1, 1, 1};
#endif
#ifdef CONFIG_MACH_AM335X_ROM3310
	static int am335x_gpio_value[] = {-1, -1, -1, -1, -1, 1, 1, 1, 1, 1};
#endif
#ifdef CONFIG_MACH_AM335X_WISE3320
	static int am335x_gpio_value[] = {-1, -1, -1, -1, 1, 1, 1, 1};
#endif
	int n = 0;

	for (n = 0; n < ARRAY_SIZE(am335x_gpio_value); ++n)
	{
		gpio_request(gpio_base + n, "PCA-953XGPIOExpander");
		if (am335x_gpio_value[n] < 0)
		{
			gpio_direction_input(gpio_base + n);
		}
		else
		{
			gpio_direction_output(gpio_base + n, am335x_gpio_value[n]);	
		}

		gpio_export(gpio_base + n, 1);
	}

	return 0;
}

struct pca953x_platform_data am335x_i2c_pca953x_platdata = {
        .gpio_base = 200,
	.invert    = 0,
	.setup = am335x_pca953x_setup,
};

#endif

static void __init am335x_evm_i2c_init(void)
{
	/* Initially assume General Purpose EVM Config */
	am335x_evm_id = GEN_PURP_EVM;

#ifdef	CONFIG_MACH_ADVANTECH
	am335x_i2c0_boardinfo[0].platform_data = &am335x_baseboard_eeprom_info;
#ifndef CONFIG_MACH_AM335X_WISE3320
	am335x_i2c0_boardinfo[2].platform_data = &am335x_i2c_pca953x_platdata;
#endif
#endif
	omap_register_i2c_bus(1, 100, am335x_i2c0_boardinfo,
				ARRAY_SIZE(am335x_i2c0_boardinfo));
}

void __iomem *am33xx_emif_base;

void __iomem * __init am33xx_get_mem_ctlr(void)
{

	am33xx_emif_base = ioremap(AM33XX_EMIF0_BASE, SZ_32K);

	if (!am33xx_emif_base)
		pr_warning("%s: Unable to map DDR2 controller",	__func__);

	return am33xx_emif_base;
}

void __iomem *am33xx_get_ram_base(void)
{
	return am33xx_emif_base;
}

void __iomem *am33xx_gpio0_base;

void __iomem *am33xx_get_gpio0_base(void)
{
	am33xx_gpio0_base = ioremap(AM33XX_GPIO0_BASE, SZ_4K);

	return am33xx_gpio0_base;
}

static struct resource am33xx_cpuidle_resources[] = {
	{
		.start		= AM33XX_EMIF0_BASE,
		.end		= AM33XX_EMIF0_BASE + SZ_32K - 1,
		.flags		= IORESOURCE_MEM,
	},
};

/* AM33XX devices support DDR2 power down */
static struct am33xx_cpuidle_config am33xx_cpuidle_pdata = {
	.ddr2_pdown	= 1,
};

static struct platform_device am33xx_cpuidle_device = {
	.name			= "cpuidle-am33xx",
	.num_resources		= ARRAY_SIZE(am33xx_cpuidle_resources),
	.resource		= am33xx_cpuidle_resources,
	.dev = {
		.platform_data	= &am33xx_cpuidle_pdata,
	},
};

static void __init am33xx_cpuidle_init(void)
{
	int ret;

	am33xx_cpuidle_pdata.emif_base = am33xx_get_mem_ctlr();

	ret = platform_device_register(&am33xx_cpuidle_device);

	if (ret)
		pr_warning("AM33XX cpuidle registration failed\n");

}

#ifdef CONFIG_MACH_AM335X_WISE3320
/* Function for setup SSC */
static void spread_spectrum_setup(const struct ssc_data* dpll_data)
{
    void __iomem* clock_base;
    struct clk* clock;
    unsigned int f;
    unsigned int fm;
    unsigned int m;
    unsigned int n;
    unsigned int ModFreqDivider;
    unsigned int Exponent;
    unsigned int Mantissa;
    unsigned int delta_m_step;

    clock_base = ioremap(AM33XX_CM_BASE + AM33XX_CM_WKUP_MOD, 0x1000);
    if (!clock_base) {
        printk(KERN_ERR "ioremap spread spectrum clocks failed\n");
        return;
    }

    /* Read PLL dividers m and n */
    m = readl(clock_base + dpll_data->clksel);
    n = m & 0x7F;
    m = (m >> 8) & 0x3FF;
    printk(KERN_INFO "%s PLL m = %d, n = %d\n", dpll_data->name, m, n);

    /* Calculate Fref */
    clock = clk_get(NULL, "sys_clkin_ck");
    f = clk_get_rate(clock);
    f = f/(1+n);
    printk(KERN_INFO "%s PLL reference clock is %dHz\n", dpll_data->name, f);

    /* Calculate max. Bandwidth (Modulation Frequency) of PLL */
    fm = f / 70;
    printk(KERN_INFO "%s PLL Bandwidth is %d\n", dpll_data->name, fm);

    /* Calculate ModFreqDivider */
    ModFreqDivider = f/(4 * fm);
    printk(KERN_INFO "%s PLL ModFreqDivider is %d\n", dpll_data->name, ModFreqDivider);

    /* Calculate Mantissa/Exponent */
    Exponent = 0;
    Mantissa = ModFreqDivider;
    while ((Mantissa > 127) && (Exponent < 7)) {
        Exponent++;
        Mantissa /= 2;
    }
    if (Mantissa > 127)
        Mantissa = 127;
    printk(KERN_INFO "%s PLL Mantissa = %d, Exponent = %d\n", dpll_data->name, Mantissa, Exponent);
    ModFreqDivider = Mantissa << Exponent;
    printk(KERN_INFO "%s PLL revised ModFreqDivider is %d\n", dpll_data->name, ModFreqDivider);

    /* Calculate Modulation steps */
    delta_m_step = (m * dpll_data->percent) << 18;
    delta_m_step /= 100;
    delta_m_step /= ModFreqDivider;
    if (delta_m_step > 0xFFFFF)
        delta_m_step = 0xFFFFF;
    printk(KERN_INFO "%s PLL Delta_M_Int = %d, Delta_M_Frac = %d\n", dpll_data->name, delta_m_step >> 18, delta_m_step & 0x3FFFF);

    /* Setup Spread Spectrum */
    writel(delta_m_step, clock_base + dpll_data->deltamstep);
    writel((Exponent << 8) | Mantissa, clock_base + dpll_data->modfreqdiv);
    m = readl(clock_base + dpll_data->clkmode);
    m &= ~0xF000;   // clear all SSC flags
    m |=  0x1000;   // enable SSC
    writel(m, clock_base + dpll_data->clkmode);
    printk(KERN_INFO "%s PLL Spread Spectrum enabled with %d percent\n", dpll_data->name, dpll_data->percent);

    iounmap(clock_base);
}
#endif

#ifdef CONFIG_MACH_ADVANTECH
extern void tps65910_power_off(void);
static void am335x_adv_poweroff(void)
{
	tps65910_power_off();
}

#ifdef CONFIG_MACH_AM335X_RSB4220
static void am335x_lvds_init()
{
	if(gpio_is_valid(GPIO_TO_PIN(3, 15)))
	{
		gpio_request(GPIO_TO_PIN(3, 15), "LCD_VDD_EN");
		gpio_direction_output(GPIO_TO_PIN(3, 15), 1);
	}else
		return;

	mdelay(10);

	if(gpio_is_valid(GPIO_TO_PIN(3, 16)))
	{
		gpio_request(GPIO_TO_PIN(3, 16), "LCD_BKLT_EN");
		gpio_direction_output(GPIO_TO_PIN(3, 16), 1);
	}else
		return;

	return;
}
#endif

#endif

static void __init am335x_evm_init(void)
{
	am33xx_cpuidle_init();
	am33xx_mux_init(board_mux);
	omap_serial_init();
	am335x_evm_i2c_init();
	omap_sdrc_init(NULL, NULL);
	usb_musb_init(&musb_board_data);
	omap_board_config = am335x_evm_config;
	omap_board_config_size = ARRAY_SIZE(am335x_evm_config);
	/* Create an alias for icss clock */
	if (clk_add_alias("pruss", NULL, "pruss_uart_gclk", NULL))
		pr_warn("failed to create an alias: icss_uart_gclk --> pruss\n");
	/* Create an alias for gfx/sgx clock */
	if (clk_add_alias("sgx_ck", NULL, "gfx_fclk", NULL))
		pr_warn("failed to create an alias: gfx_fclk --> sgx_ck\n");

#ifdef CONFIG_MACH_AM335X_ROM3310 
	pm_power_off = am335x_adv_poweroff;
#endif

#ifdef CONFIG_MACH_AM335X_WISE3320
	spread_spectrum_setup(&per_dpll_data);
#endif

#ifdef CONFIG_MACH_AM335X_RSB4220
	am335x_lvds_init();
#endif
	
}

static void __init am335x_evm_map_io(void)
{
	omap2_set_globals_am33xx();
	omapam33xx_map_common_io();
}

MACHINE_START(AM335XEVM, "am335xevm")
	/* Maintainer: Texas Instruments */
	.atag_offset	= 0x100,
	.map_io		= am335x_evm_map_io,
	.init_early	= am33xx_init_early,
	.init_irq	= ti81xx_init_irq,
	.handle_irq     = omap3_intc_handle_irq,
	.timer		= &omap3_am33xx_timer,
	.init_machine	= am335x_evm_init,
MACHINE_END

MACHINE_START(AM335XIAEVM, "am335xiaevm")
	/* Maintainer: Texas Instruments */
	.atag_offset	= 0x100,
	.map_io		= am335x_evm_map_io,
	.init_irq	= ti81xx_init_irq,
	.init_early	= am33xx_init_early,
	.timer		= &omap3_am33xx_timer,
	.init_machine	= am335x_evm_init,
MACHINE_END
