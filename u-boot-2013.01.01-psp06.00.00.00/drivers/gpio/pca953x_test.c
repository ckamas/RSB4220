

#define AM335X    1

#ifndef  AM335X //---------------------for Linux -----------------
//#include <stdio.h>

typedef    unsigned char    uint8_t;
typedef    unsigned int     uint;

uint8_t _ext_pin_status;

int pca953x_set_val(uint8_t chip, uint mask, uint data)
{
  uint8_t reg;

  reg = _ext_pin_status;
  reg = reg & ~mask;

  reg |= data;
  /* do write ... */
  _ext_pin_status = reg;

  /* make high 4 bits => transfer to low 4 bit */
  reg = (reg>>4) & 0xf;
  _ext_pin_status = (_ext_pin_status&0xf0) | reg;

  return 0;
}

int pca953x_get_val(uint8_t chip)
{
  return (int)_ext_pin_status;
}

#endif   //---------------------------------------------------
typedef    unsigned char    uint8_t;
typedef    unsigned int     uint;

uint8_t _ext_pin_status;

extern int pca953x_set_val(uint8_t chip, uint mask, uint data);
extern int pca953x_get_val(uint8_t chip);
//yuechao
extern int pca953x_set_dir(uint8_t chip, uint mask, uint data);
//yuechao init cfg
int pca953x_cfg(uint8_t chip, uint mask, uint data)
{
  pca953x_set_dir(chip, mask, data);
  return 0;
}

int pca953x_test_out(uint8_t chip, uint mask, uint8_t data)
{
  pca953x_set_val(chip, mask, data);
  return 0;
}

int pca953x_test_in(uint8_t chip, uint8_t *pdata)
{
  int val = pca953x_get_val(chip);
  *pdata = (uint8_t)val;
  //printf("\n => %x %x\n", val, *pdata);

  return 0;
}

#if !AM335X   //--------------- for Linux ------------------------
 int main(void)
#else  // ------------ for UBOOT --------------

#include <common.h>
#include <i2c.h>
#include <pca953x.h>
//yuechaocfg
int my_cfg(uint8_t chip)
{
 int i;
 for(i=4; i<8; i++){
 pca953x_cfg(chip, 1<<i, 0<<i);
 }
}
//my_main ok!
int my_main(uint8_t chip)
{
  int     i;
  uint8_t result;
  // run 0x00 -> 0xf0 -> 0x00
  
  // make all output high 4 bit = 0
  for(i=4; i<8; i++) {
    pca953x_test_out(chip, 1<<i, 0<<i);
  }
  // read back  
  pca953x_test_in(chip, &result);
  result &= 0xf;
  if(result != 0) {
    printf("ERR\n");
  }
  else
    printf("make 0 pass\n");

  
  // make all output high 4 bit = 1
  for(i=4; i<8; i++) {
    pca953x_test_out(chip, 1<<i, 1<<i);
  }
  // read back
  pca953x_test_in(chip, &result);
  result &= 0xf;
  if(result != 0xf) {
    printf("ERR 0x%x\n", result);
  }
  else
    printf("make 1 pass\n");

  // make all output high 4 bit = 0
  for(i=4; i<8; i++) {
    pca953x_test_out(chip, 1<<i, 0<<i);
  }
  // read back
  pca953x_test_in(chip, &result);
  result &= 0xf;
  if(result != 0) {
    printf("ERR\n");
  }
  else
    printf("make 0 pass\n");

  // ---- walk zero ------------
  // TODO .... 

  return 0;
}
#endif


