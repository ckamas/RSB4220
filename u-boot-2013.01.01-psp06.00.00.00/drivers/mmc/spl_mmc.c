/*
 * (C) Copyright 2010
 * Texas Instruments, <www.ti.com>
 *
 * Aneesh V <aneesh@ti.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */
#include <common.h>
#include <spl.h>
#include <asm/u-boot.h>
#include <asm/utils.h>
#include <mmc.h>
#include <fat.h>
#include <version.h>

DECLARE_GLOBAL_DATA_PTR;

static void mmc_load_image_raw(struct mmc *mmc)
{
	u32 image_size_sectors, err;
	const struct image_header *header;

	header = (struct image_header *)(CONFIG_SYS_TEXT_BASE -
						sizeof(struct image_header));

	/* read image header to find the image size & load address */
	err = mmc->block_dev.block_read(0,
			CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR, 1,
			(void *)header);

	if (err <= 0)
		goto end;

	spl_parse_image_header(header);

	/* convert size to sectors - round up */
	image_size_sectors = (spl_image.size + mmc->read_bl_len - 1) /
				mmc->read_bl_len;

	/* Read the header too to avoid extra memcpy */
	err = mmc->block_dev.block_read(0,
			CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR,
			image_size_sectors, (void *)spl_image.load_addr);

end:
	if (err <= 0) {
		printf("spl: mmc blk read err - %d\n", err);
		hang();
	}
}

#ifdef CONFIG_SPL_FAT_SUPPORT
static void mmc_load_image_fat(struct mmc *mmc)
{
	s32 err;
	struct image_header *header;

	header = (struct image_header *)(CONFIG_SYS_TEXT_BASE -
						sizeof(struct image_header));

	err = fat_register_device(&mmc->block_dev,
				CONFIG_SYS_MMC_SD_FAT_BOOT_PARTITION);
	if (err) {
		printf("spl: fat register err - %d\n", err);
		hang();
	}

	err = file_fat_read(CONFIG_SPL_FAT_LOAD_PAYLOAD_NAME,
				(u8 *)header, sizeof(struct image_header));
	if (err <= 0)
		goto end;

	spl_parse_image_header(header);

	err = file_fat_read(CONFIG_SPL_FAT_LOAD_PAYLOAD_NAME,
				(u8 *)spl_image.load_addr, 0);

end:
	if (err <= 0) {
		printf("spl: error reading image %s, err - %d\n",
			CONFIG_SPL_FAT_LOAD_PAYLOAD_NAME, err);
		hang();
	}
}
#endif

#ifdef CONFIG_ADVANTECH
static int judge_sd_is_insert(void)
{
	struct mmc *mmc;
	int err;
	int sd_have_is = 1; /*no insert SD & SD have no data*/


	mmc_adv_initialize(gd->bd, 0);
	mmc = find_mmc_device(0);
	err = mmc_init(mmc);
	if(UNUSABLE_ERR == err)/*timedout waiting no insert SD Card(-17)*/
		sd_have_is = 0;
	return sd_have_is;
}

#define BOOTFROMEMMC0 0
#define BOOTFROMEMMC1 1

static int get_boot_select(void)
{
#define GPIO_TO_PIN(bank, gpio) (32 * (bank) + (gpio))
	unsigned char BootSel0 = 0;
	unsigned char BootSel1 = 0;
	unsigned char BootSel2 = 0;

	BootSel0 = gpio_get_value(GPIO_TO_PIN(0, 20));
	BootSel1 = gpio_get_value(GPIO_TO_PIN(0, 28));
	BootSel2 = gpio_get_value(GPIO_TO_PIN(3, 21));

	printf("Boot0: GPIO0_20 = %d\n", BootSel0);
	printf("Boot1: GPIO0_28 = %d\n", BootSel1);
	printf("Boot2: GPIO3_21 = %d\n", BootSel2);

	if(1 == BootSel0 && 0 == BootSel1 && 0 == BootSel2)
		return BOOTFROMEMMC0;

	if(0 == BootSel0 && 1 == BootSel1 && 0 == BootSel2)
		return BOOTFROMEMMC1;

	return BOOTFROMEMMC0;
}
#endif

void spl_mmc_load_image(void)
{
	struct mmc *mmc;
	int err;
	u32 boot_mode;

	mmc_initialize(gd->bd);
	/* We register only one device. So, the dev id is always 0 */
	mmc = find_mmc_device(0);
	if (!mmc) {
		puts("spl: mmc device not found!!\n");
		hang();
	}

	err = mmc_init(mmc);
	if (err) {
		printf("spl: mmc init failed: err - %d\n", err);
		hang();
	}
	boot_mode = spl_boot_mode();
	if (boot_mode == MMCSD_MODE_RAW) {
		debug("boot mode - RAW\n");
		mmc_load_image_raw(mmc);
#ifdef CONFIG_SPL_FAT_SUPPORT
	} else if (boot_mode == MMCSD_MODE_FAT) {
		debug("boot mode - FAT\n");
		mmc_load_image_fat(mmc);
#endif
	} else {
		puts("spl: wrong MMC boot mode\n");
		hang();
	}
#ifdef CONFIG_ADV_LOADER
	/*make sure boot from SD, u-boot boot Kernel is SD*/
	switch(spl_boot_device())
	{
		case BOOT_DEVICE_MMC1:
			*(int *)0XC1000000 = 0X0;
			break;
		case BOOT_DEVICE_MMC2:
			if(judge_sd_is_insert())
			{
				*(int *)0XC1000000 = 0X1;
			}else
			{
				*(int *)0XC1000000 = 0X2;
			}
			break;
		default:
			*(int *)0XC1000000 = 0X0;
	}
#endif
}

#ifdef CONFIG_ADV_LOADER
#define MMC_DEVICE_MAX 2
#define FALSE 	(0)
#define TRUE 	(1)

static int mmc_adv_load_image_fat(struct mmc *mmc)
{
	s32 err;
	struct image_header *header;

	header = (struct image_header *)(CONFIG_SYS_TEXT_BASE -
						sizeof(struct image_header));

	err = fat_register_device(&mmc->block_dev,
				CONFIG_SYS_MMC_SD_FAT_BOOT_PARTITION);
	if (err) {
		printf("spl: fat register err - %d\n", err);
		return FALSE;
	}

	err = file_fat_read(CONFIG_SPL_FAT_LOAD_PAYLOAD_NAME,
				(u8 *)header, sizeof(struct image_header));
	if (err <= 0)
		goto end;

	spl_parse_image_header(header);

	err = file_fat_read(CONFIG_SPL_FAT_LOAD_PAYLOAD_NAME,
				(u8 *)spl_image.load_addr, 0);

end:
	if (err <= 0) {
		printf("spl: error reading image %s, err - %d\n",
			CONFIG_SPL_FAT_LOAD_PAYLOAD_NAME, err);
		return FALSE;
	}else
	{
		return TRUE;
	}
}

void spl_adv_load_image(void)
{
	struct mmc *mmc;
	int err;
	u32 boot_mode;
	int index = 0;
	int sd_have_is = 1; /*no insert SD & SD have no data*/

#ifdef CONFIG_MEM_ROM3310
	int device_num = 0;
	index = get_boot_select();
	device_num = index? MMC_DEVICE_MAX : (MMC_DEVICE_MAX - 1);
	for(; index < device_num; index++)
#else
	for(index = 0; index < MMC_DEVICE_MAX; index++)
#endif
	{
		mmc_adv_initialize(gd->bd, index);
		/* We register only one device. So, the dev id is always 0 */
		mmc = find_mmc_device(0);
		if(!mmc){
			puts("spl: mmc device not found!!\n");
			continue;
		}

		err = mmc_init(mmc);
		if (err){
			printf("spl: mmc init failed: err - %d\n", err);
			if(UNUSABLE_ERR == err)
			{
				sd_have_is = 0; /*no insert sd*/
			}
			continue;
		}

		boot_mode = spl_boot_mode();
		if (boot_mode == MMCSD_MODE_RAW) {
			debug("boot mode - RAW\n");
			mmc_load_image_raw(mmc);
#ifdef CONFIG_SPL_FAT_SUPPORT
		} else if (boot_mode == MMCSD_MODE_FAT) {
			debug("boot mode - FAT\n");
			err = mmc_adv_load_image_fat(mmc);
			if(err > 0){
				break;
			}
#endif
		} else {
			puts("spl: wrong MMC boot mode\n");
			hang();
		}
	}

	/*Add flag for u-boot boot Kernel*/
	*(int *)0XC1000000 = index;
	if(!sd_have_is)
	{
		*(int *)0XC1000000 = 0X02;
	}
#ifdef CONFIG_MEM_ROM3310
	if(!judge_sd_is_insert())
	{
		*(int *)0XC1000000 = 0X02;
	}
#endif

	if(index >= MMC_DEVICE_MAX && !err){
		hang();
	}
}
#endif
