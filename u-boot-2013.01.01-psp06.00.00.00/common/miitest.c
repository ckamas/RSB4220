//test functions for mii
static void extract_rang(
        char * input,
        unsigned char * plo,
        unsigned char * phi)
{
        char * end;
        *plo = simple_strtoul(input, &end, 16);
        if (*end == '-') {
                end++;
                *phi = simple_strtoul(end, NULL, 16);
        }
        else {
                *phi = *plo;
        }
}

void my_test(char op[3], unsigned char *addrlo, unsigned char *addrhi,
                     unsigned char *reglo,  unsigned char *reghi,
                     unsigned char *addr,   unsigned char *reg,
                     unsigned short *data,  int           *rcode,
                     const char    *(*devname), char addresst[][16], 
                     char regt[][16], char datat[][16], int cmd_num )

{
 int i;
 printf ("mii test start!\n");
 for(i=0;i<cmd_num;i++){
//for(i=0;i<=5;i++){   
              extract_rang(addresst[0], addrlo, addrhi);
                 extract_rang(regt[i], reglo, reghi);
                 *data = simple_strtoul (datat[i], NULL, 16);
                 for (*addr = *addrlo; *addr <= *addrhi; (*addr)++){
                 for (*reg = *reglo; *reg <= *reghi; (*reg)++){
                        printf("0x%x,0x%x,0x%x\n" ,*addr, *reg ,*data);
                      if (miiphy_write (*devname, *addr, *reg, *data) != 0 ) {
                           printf("Error Occurs in Tests");
                          *rcode = 1;
                         }
                     }
                   }
                }
 printf ("mii test over\n");
}
void test(char op[3],unsigned char *addrlo, unsigned char *addrhi,
		     unsigned char *reglo,  unsigned char *reghi,
		     unsigned char *addr,   unsigned char *reg,
		     unsigned short *data,  int           *rcode,
		     const char    *(*devname))
{
if (strncmp(op,"t40", 3) == 0) {
                char addresst[1][16] ={"0x04"};
                char regt[6][16] ={"0x10","0x00","0x1d","0x1e","0x1d","0x1e"};
                char datat[6][16]={"0x0800","0xA100","0x0029","0x36DC","0x000B","0x3C40"};
		int cmd_num = 6;
		my_test(op,addrlo, addrhi, reglo, reghi, addr, reg, data, rcode, devname, addresst, regt, datat,cmd_num);}
                 
else if (strncmp(op,"t50", 3) == 0) {
                char addresst[1][16] ={"0x05"};
                char regt[6][16] ={"0x10","0x00","0x1d","0x1e","0x1d","0x1e"};
                char datat[6][16]={"0x0800","0xA100","0x0029","0x36DC","0x000B","0x3C40"};
                int cmd_num = 6;
		my_test(op,addrlo, addrhi, reglo, reghi, addr, reg, data, rcode, devname, addresst, regt, datat, cmd_num);}

else if (strncmp(op,"t41", 3) == 0){
                char addresst[1][16] ={"0x04"};
                char regt[4][16] ={"0x1d","0x1e","0x00","0x09"};
                char datat[4][16]={"0x000b","0x0009","0x8140","0x2200"};
                int cmd_num = 4;
		my_test(op, addrlo, addrhi, reglo, reghi, addr, reg, data, rcode, devname, addresst, regt, datat, cmd_num);}

else if (strncmp(op,"t51", 3) == 0) {
                char addresst[1][16] ={"0x05"};
                char regt[4][16] ={"0x1d","0x1e","0x00","0x09"};
                char datat[4][16]={"0x000b","0x0009","0x8140","0x2200"};
                int cmd_num = 4;
		my_test(op, addrlo, addrhi, reglo, reghi, addr, reg, data, rcode, devname, addresst, regt, datat,cmd_num);}

else if (strncmp(op,"t44", 3) == 0)
                {
                char addresst[1][16] ={"0x04"};
                char regt[4][16] ={"0x1d","0x1e","0x00","0x09"};
                char datat[4][16]={"0x000b","0x0009","0x8140","0x8200"};
                int cmd_num = 4;
		my_test(op,addrlo, addrhi, reglo, reghi, addr, reg, data, rcode, devname, addresst, regt, datat, cmd_num);}

else if (strncmp(op,"t54", 3) == 0)
   {
                char addresst[1][16] ={"0x05"};
                char regt[4][16] ={"0x1d","0x1e","0x00","0x09"};
                char datat[4][16]={"0x000b","0x0009","0x8140","0x8200"};
                int cmd_num = 4;
		my_test(op,addrlo, addrhi, reglo, reghi, addr, reg, data, rcode, devname, addresst, regt, datat, cmd_num);
	}
else { printf("please type:mii t40(test phy 4 100mb/s),\n");
       printf("please type:mii t50(test phy 5 100mb/s),\n");
       printf("please type:mii t41(test phy 4 mode 1 1000mb/s),\n");
       printf("please type:mii t51(test phy 5 mode 1 1000mb/s),\n");
       printf("please type:mii t44(test phy 4 mode 4 1000mb/s),\n");
       printf("please type:mii t54(test phy 5 mode 4 1000mb/s).\n");
     }
}
